import 'package:flutter/material.dart';
// import 'package:flutter_dev/models/models.dart';
import 'package:flutter_dev/database/db_provider.dart';
import 'package:flutter_dev/models/occupation.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:autocomplete_textfield/autocomplete_textfield.dart';

class LinccupsListPage extends StatefulWidget {
  _LinccupsListPageState createState() => _LinccupsListPageState();
}

class _LinccupsListPageState extends State<LinccupsListPage> {
  GlobalKey key = new GlobalKey<AutoCompleteTextFieldState<Occupation>>();
  AutoCompleteTextField<Occupation> textField;  

  List<Occupation> _newData = [];
  List<Occupation> _occupations = [];
  Occupation selected;

  _onChanged(String value) {
    if(textField != null)
      textField.textField.controller.text = value;

    setState(() {
      _newData = _occupations
          .where((o) => o.occupationName.toLowerCase().contains(value.toLowerCase()))
          .toList();
    });
  }

  _LinccupsListPageState() {
    textField = new AutoCompleteTextField<Occupation>(
      decoration: new InputDecoration(
        border: OutlineInputBorder(), hintText: "Search", suffixIcon: new Icon(Icons.search)),
      itemSubmitted: (item) => _onChanged(item.occupationName),
      key: key,
      suggestions: _occupations,
      itemBuilder: (context, suggestion) => new Padding(
          child: new ListTile(
              title: new Text(suggestion.occupationName)),
          padding: EdgeInsets.all(8.0)),
      itemSorter: (a, b) => a.sortSequence == b.sortSequence ? 0 : a.sortSequence > b.sortSequence ? -1 : 1,
      itemFilter: (suggestion, input) =>
          suggestion.occupationName.toLowerCase().startsWith(input.toLowerCase()),
      clearOnSubmit: false,
    );
  }

  @override
  void initState(){
    super.initState();
    DbProvider().getAllOccupations().then((value) {
      setState(() {
        _occupations = value;
      });
      textField.updateSuggestions(value);
      _onChanged('');
    });
  }  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          SizedBox(height: 15.0),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: textField,
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: Padding(padding: const EdgeInsets.only(left: 30, top: 3), child: Text('Names, Categories')),
          ),
          SizedBox(height: 5.0),
          _newData != null && _newData.length != 0
              ? Expanded(
                  child: ListView(
                    padding: EdgeInsets.all(10.0),
                    children: _newData.map((data) {
                      return Card(
                        child: data.selectable == 'T' ? 
                          ListTile(
                            leading: FlutterLogo(size: 30.0),
                            title: Text(data.occupationName),
                            subtitle: Text('9:30AM @ Veranda Coffee'),
                            trailing: IconButton(
                              icon: Icon(FontAwesomeIcons.solidEdit),
                              onPressed: (){
                              },
                            ),
                          ) : ListTile(
                            leading: FlutterLogo(size: 30.0),
                            title: Text(data.occupationName),
                            trailing: IconButton(
                              icon: Icon(FontAwesomeIcons.solidHeart),
                              onPressed: (){
                              },
                            ),
                          ),
                      );
                    }).toList(),
                  ),
                )
              : SizedBox(),
        ],
      ),
    );
  }
}