import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';

class CarouselWidgetSlider extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return CarouselSlider(
        aspectRatio: 16/9,
        height: 130.0,
        items: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.symmetric(horizontal: 5.0),
            decoration: BoxDecoration(
              color: Colors.blue[400],
              borderRadius: BorderRadius.all(Radius.circular(13.0)),
            ),
            child: Stack(
              children: <Widget>[
                ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(8.0)),
                    child: Image.asset('assets/images/AdobeStock_58705368-two-cups-coffee.jpeg',width: 400.8,fit: BoxFit.cover,)),
                Container(
                  margin: EdgeInsets.only(left: 5.0, bottom: 5.0),
                  child: Align(
                    alignment: Alignment.bottomLeft,
                    child: Text('Coffee', style: TextStyle(fontSize: 26.0, fontWeight: FontWeight.bold, color: Colors.white, shadows: [Shadow(blurRadius: 3.0)],),),
              ),
                ),]
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.symmetric(horizontal: 5.0),
            padding: EdgeInsets.only(bottom: 10.0),
            decoration: BoxDecoration(
              color: Colors.blue[400],
              borderRadius: BorderRadius.all(Radius.circular(13.0)),
            ),
            child: Align(
              alignment: Alignment.bottomCenter,
              child: Text('Drinks', style: TextStyle(fontSize: 26.0, color: Colors.white70),),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.symmetric(horizontal: 5.0),
            padding: EdgeInsets.only(bottom: 10.0),
            decoration: BoxDecoration(
              color: Colors.blue[400],
              borderRadius: BorderRadius.all(Radius.circular(13.0)),
            ),
            child: Align(
              alignment: Alignment.bottomCenter,
              child: Text('Meet & Greet', style: TextStyle(fontSize: 26.0, color: Colors.white70),),
            ),
          ),
        ],
    );
  }
}

class CarouselWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 69.8,
      child:ListView(
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          Container(
            width: 100.0,
            color: Colors.red,
            child: IconButton(icon: Icon(Icons.business), onPressed: null,),
          ),
          Container(
            width: 100.0,
            color: Colors.blue,
            child: IconButton(icon: Icon(Icons.forum), onPressed: null,),
          ),
          IconWidget(itemColor: Colors.red),
          Container(
            width: 100.0,
            color: Colors.yellow,
            child: IconButton(icon: Icon(Icons.home), onPressed: null,),
          ),
          Container(
            width: 100.0,
            color: Colors.orange,
            child: IconButton(icon: Icon(Icons.hot_tub), onPressed: null,),
          ),
        ],
      ),
    );
  }
}

class IconWidget extends StatelessWidget {
  final Color itemColor;

  const IconWidget({
    @required this.itemColor,
  }) : assert(itemColor != null);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
          width: 100.0,
          color: itemColor,
          child: Column(
            children: <Widget>[
              IconButton(icon: Icon(Icons.file_upload), onPressed: null,),
              Text("Coffee"),
            ],
          ),
    );
  }
}