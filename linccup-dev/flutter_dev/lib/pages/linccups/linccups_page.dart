import 'package:flutter/material.dart';
import 'package:flutter_dev/pages/linccups/linccups_list_page.dart';
import 'package:flutter_dev/widgets/menu.dart';
import 'package:bmnav/bmnav.dart' as bmnav;
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'digest_page.dart';

class LinccupsPage extends StatefulWidget {
  @override
  _LinccupsPageState createState() => _LinccupsPageState();
}

class _LinccupsPageState extends State<LinccupsPage> {
  int _currentIndex = 0;
  final List<Widget> _children = [
    new LinccupsListPage(),
    new DigestPage(),
    Text('Schedule'),
    Text('Contacts'),
  ];
  
  @override
  initState() {
    super.initState();   
  }  

  void onTabTapped(int index) {
   setState(() {
     _currentIndex = index;
   });
 }  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Linccups"),
        actions: <Widget>[
          IconButton(
            icon: Icon(FontAwesomeIcons.slidersH),
            onPressed: (){
              Navigator.pushNamed(context, '/settings');
            },
          ),
        ],
      ),
      drawer: Menu(),
      body: Center(
        child: _children[_currentIndex],
      ),
      bottomNavigationBar: bmnav.BottomNav(
        color: Color(0xFF6100ed),
        labelStyle: bmnav.LabelStyle(
          showOnSelect: true, 
          onSelectTextStyle: 
          TextStyle(color: Colors.white, height: 1.5)
        ),
        iconStyle: bmnav.IconStyle(
          color: Color(0xFFbb93f6), 
          onSelectSize: 32.0, 
          onSelectColor: Colors.white
        ),
        items: [
          bmnav.BottomNavItem(
            FontAwesomeIcons.bullseye,
            label: 'Digest',
          ),
          bmnav.BottomNavItem(
            FontAwesomeIcons.solidUserCircle,
            label: 'Users',
          ),
          bmnav.BottomNavItem(
            FontAwesomeIcons.calendarDay,
            label: 'Schedule',
          ),
          bmnav.BottomNavItem(
            FontAwesomeIcons.solidEnvelope,
            label: 'Contacts',
          ),
        ],
        index: _currentIndex,
        // type: BottomNavigationBarType.fixed,
        // backgroundColor: Color(0xFF6100ed),
        // selectedItemColor: Colors.white,
        // unselectedLabelStyle: TextStyle(fontSize: 0),
        // unselectedItemColor: Color(0xFFbb93f6),
        onTap: onTabTapped,
      )
    );
  }
}