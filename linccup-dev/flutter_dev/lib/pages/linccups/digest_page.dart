import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_dev/pages/linccups/carousel_widget.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

class DigestPage extends StatefulWidget {
  @override
  _DigestPageState createState() => _DigestPageState();
}

class _DigestPageState extends State<DigestPage> {
  Completer<GoogleMapController> _controller = Completer();
  var location = new Location();
  LocationData userLocation;

  _DigestPageState() {
    
  }

  @override
  initState() {
    super.initState();   
    getLocation();
  }    

  getLocation() async {
    try {
      userLocation = await location.getLocation();
    } on PlatformException catch (e) {
      userLocation = null;
    }    

    location.onLocationChanged().listen((LocationData currentLocation) {
      setState(() {
        userLocation = currentLocation;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: Stack(
        children: [
          userLocation == null
            ? Center(child: CircularProgressIndicator())
            : GoogleMap(
                mapType: MapType.normal,
                initialCameraPosition: CameraPosition(
                  target: LatLng(userLocation.latitude, userLocation.longitude),
                  zoom: 14.4746,
                ),
                onMapCreated: (GoogleMapController controller) {
                  _controller.complete(controller);
                },
                circles: Set.from([Circle(
                  circleId: CircleId('circled'),
                  center: LatLng(userLocation.latitude, userLocation.longitude),
                  radius: 600,
                  strokeWidth: 2,
                  strokeColor: Color(0xFFb23842),
                ),
                Circle(
                  circleId: CircleId('centered'),
                  center: LatLng(userLocation.latitude, userLocation.longitude),
                  radius: 60,
                  fillColor: Color(0xFF001379),
                  strokeWidth: 10,
                  strokeColor: Color(0xFF474fab),
                )])
              ),
          Positioned(
            top: 20,
            child: CarouselWidget()
          )
        ]
      ),
    );
  }

  Future<Map<String, double>> _getLocation() async {
    var currentLocation = <String, double>{};
    try {
      currentLocation = (await location.getLocation()) as Map<String, double>;
    } catch (e) {
      currentLocation = null;
    }
    return currentLocation;
  }
}