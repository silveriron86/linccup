import 'package:flutter/material.dart';
import 'package:flutter_dev/database/db_provider.dart';
import 'package:flutter_dev/models/basic_settings.dart';

class BasicSettingsPage extends StatefulWidget {
  @override
  _BasicSettingsPageState createState() => _BasicSettingsPageState();
}

class _BasicSettingsPageState extends State<BasicSettingsPage> {
  Map<String, dynamic> _formData = Map<String, dynamic>();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final String myUserId = 'TEST1';

  @override
  initState() {
    super.initState();
    init();
  }

  init() async {
    var isExist = await DbProvider().checkBasicSettings(myUserId);
    if(isExist == null) {
      DbProvider().newBasicSettings(BasicSettings(userId: myUserId, name: '', incognitoSetting: true, notificationsSetting: true));
    }    
  }

  @override
  Widget build(BuildContext context) {
    _formData['notifications'] = false;
    _formData['incognito'] = false;

    return Scaffold(
        appBar: AppBar(
          title: const Text("Settings"),
        ),
        body: Form(
          key: _formKey,
          child: Container(
            margin: const EdgeInsets.only(
              left: 20.0,
              top: 50.0,
              right: 20.0,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Text("Basic Settings"),
                TextFormField(
                  decoration: InputDecoration(
                      labelText: 'Name',

                  ),
                  validator: (value) => value.isEmpty ? 'Required' : null,
                  onSaved: (value) => _formData['name'] = value,
                ),
                SwitchListTile(
                  title: const Text("Notifications"),
                  value: _formData['notifications'],
                  onChanged: (bool value){_formData['notifications'] = value;},
                  secondary: const Icon(Icons.notifications),
                ),
                SwitchListTile(
                  title: const Text("Incognito"),
                  value: _formData['incognito'],
                  onChanged: (bool value){_formData['incognito'] = value;},
                  secondary: const Icon(Icons.gps_off),
                )
              ],
            ),
          ),
        ),
    );
  }
}
