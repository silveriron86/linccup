import 'package:flutter/material.dart';
import 'package:flutter_dev/widgets/menu.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Linccup Home"),
      ),
      drawer: Menu(),
      body: Container(
        child: Text('Home Page'),
      ),
    );
  }
}

