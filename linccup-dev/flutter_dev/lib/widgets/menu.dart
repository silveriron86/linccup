import 'package:flutter/material.dart';

class Menu extends StatelessWidget {
  const Menu({Key key}) : super(key: key);


  @override
  Widget build(BuildContext context) {

    var color = Theme.of(context).primaryColor;

    return SizedBox(
      width: 200,
      child: Drawer(
        child: ListView(
          children: <Widget>[
            UserAccountsDrawerHeader(
              onDetailsPressed: () => Navigator.of(context).pushNamed('/home'),
              accountEmail: Text("mac@linccup.com"),
              accountName: Text("Mac Chambers"),
              currentAccountPicture: CircleAvatar(
                backgroundImage: NetworkImage('https://www.thefamouspeople.com/profiles/images/bill-gates-5.jpg'),
              ),
            ),

            ListTile(
              leading: Icon(Icons.link),
              title: Text('Linccups'),
              onTap: ()=> Navigator.of(context).pushNamed('/linccups'),
            ),
            ListTile(
              leading: Icon(Icons.message),
              title: Text('Messages'),
              onTap: ()=> Navigator.of(context).pushNamed('/messages'),
            ),
            ListTile(
              leading: Icon(Icons.settings),
              title: Text('Settings'),
              onTap: ()=> Navigator.of(context).pushNamed('/settingsremote'),
            ),
          ],
        ),
      ),
    );
  }

  // onTap: ()=> Navigator.of(context).pushNamed('/route'),
  void onNavigation(BuildContext context, String uri){
    Navigator.of(context).pop();
    Navigator.of(context).pushNamed(uri);
  }
}

class _MenuItem extends StatelessWidget {
  const _MenuItem({
    Key key,
    @required this.color,
    @required this.title,
    @required this.icon,
    @required this.onTap,
  }) : super(key: key);

  final Color color;
  final String title;
  final IconData icon;
  final Function onTap;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Opacity(
        opacity: 0.6,
        child: Container(
          height: 70,
          alignment: Alignment.center,
          child: Column(
            children: <Widget>[
              Icon(
                icon,
                color: color,
                size: 50.0,
              ),
              Text(
                title,
                style: TextStyle(
                  color: color,
                  fontSize: 14.0,
                  fontWeight: FontWeight.w500,
                ),
              ),

            ],
          ),
        ),
      ),
    );
  }
}