import 'package:flutter/material.dart';
import 'package:flutter_dev/pages/contacts/contacts_list_page.dart';
import 'package:flutter_dev/pages/linccups/linccups_page.dart';
import 'package:flutter_dev/pages/messsages/messages_list_page.dart';
import 'package:flutter_dev/pages/settings/basic_setting.dart';
import './pages/index.dart';


final routes ={
  '/': (_) => LoginPage(),
  '/home': (_)=> HomePage(),
  '/linccups': (_)=> LinccupsPage(),
  '/contacts': (_)=> ContactsListPage(),
  '/messages': (_)=> MessagesListPage(),
  '/settings': (_)=> BasicSettingsPage(),
};

final RouteObserver<PageRoute> routeObserver = RouteObserver<PageRoute>();