import 'dart:convert';
import 'dart:io';
import 'dart:async';
import 'package:flutter_dev/models/basic_settings.dart';
import 'package:flutter_dev/models/occupation.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter/services.dart' show rootBundle;

class DbProvider {
  Database _database;

  Future<Database> get database async{
    if(_database == null){
      _database = await _initialize();
    }
    return _database;
  }

  void dispose(){
    _database?.close();
    _database = null;
  }

  Future<Database> _initialize() async {
    Directory docDir = await getApplicationDocumentsDirectory();
    String path = join(docDir.path, 'linccup.db');
    print(docDir.path);
    Database db = await openDatabase(
        path,
        version: 1,
        onOpen: (db){
          print('Database Open!');
        },
        onCreate: _onCreate,
    );
    return db;
  }

  Future<String> getFileData(String path) async {
    return await rootBundle.loadString(path);
  }  

  Future<void> _importOccupations(db) async {
    String data = await getFileData('assets/res/oe.occupation.txt');
    int idx = 0;
    var row = [];
    Occupation occupation;
    LineSplitter.split(data).forEach((l) async => {
      if (idx > 0) {
        row = l.split('\t'),
        occupation = new Occupation(id: null, occupationCode: row[0], occupationName: row[1], displayLevel: int.parse(row[2]), selectable: row[3], sortSequence: int.parse(row[4])),
        await db.insert('Occupations', occupation.toMap())
      },
      idx++
    });
  }

  Future<Database> createDatabase() async {
    final db = await database;
    return db;
  }

  void _onCreate(Database db, int version) async{
    await db.execute("CREATE TABLE Occupations ("
        "id INTEGER PRIMARY KEY,"
        "occupation_code TEXT,"
        "occupation_name TEXT,"
        "display_level INTEGER,"
        "selectable TEXT,"
        "sort_sequence INTEGER"
        ")");

    await db.execute("CREATE TABLE BasicSettings ("
        "userId TEXT PRIMARY KEY,"
        "name TEXT,"
        "notificationsSetting INTEGER,"
        "incognitoSetting INTEGER"
        ")");        
    
    _importOccupations(db);
    print("Success: Import DB");
  }

  Future<List<Occupation>> getAllOccupations() async {
    final db = await database;
    var res = await db.query('Occupations');
    List<Occupation> list = res.isNotEmpty ? res.map((l) => Occupation.fromMap(l)).toList() : [];
    return list;
  }  

  checkBasicSettings(String userId) async {
    final db = await database;
    var res = await db.query("BasicSettings", where: "userId = ?", whereArgs: [userId]);
    return res.isNotEmpty ? BasicSettings.fromMap(res.first) : Null ;
  }  

  newBasicSettings(BasicSettings basicSettings) async {
    final db = await database;
    var res = await db.insert("BasicSettings", basicSettings.toMap());
    return res;
  }

  updateBasicSettings(BasicSettings basicSettings) async {
    final db = await database;
    var res = await db.update("BasicSettings", basicSettings.toMap(),
        where: "userId = ?", whereArgs: [basicSettings.userId]);
    return res;
  }  
}
