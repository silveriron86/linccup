import 'package:meta/meta.dart';
import '../uuid.dart';

@immutable
class Linccup {
  final int id;
  final bool complete;
  final List<String> userIDs;
  final String title;
  final String creatorUserID;
  final String dateTime;
  final String location;

  Map<String, dynamic> toMap() => {
    'id':id,
    'complete': complete,
    'userIDs': userIDs,
    'creatorUserID': creatorUserID,
    'title': title,
    'dateTime': dateTime,
    'location': location,
  };

  factory Linccup.fromMap(Map<String, dynamic> map) => Linccup(
    id: map['id'],
    complete: map['complete'],
    userIDs: map['userIDs'],
    creatorUserID: map['creatorUserID'],
    title: map['title'],
    dateTime: map['dateTime'],
    location: map['location'],
  );

  Linccup({@required this.id, this.complete = false, String location = '', List<String> userIDs,  String creatorUserID, String dateTime, String title})
      : this.userIDs = userIDs ?? ['',''],
        this.creatorUserID = creatorUserID ?? Uuid().generateV4(),
        this.title = title ?? '',
        this.location = location ?? '',
        this.dateTime = dateTime ?? '';

  Linccup.fromData(Map<String, dynamic> data)
      : id = data['id'],
        complete = data['complete'],
        userIDs = data['userIDs'],
        creatorUserID = data['creatorUserID'],
        dateTime = data['dateTime'],
        location = data['location'],
        title = data['title'];
}