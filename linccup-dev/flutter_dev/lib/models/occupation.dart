import 'package:flutter/cupertino.dart';
import 'package:meta/meta.dart';

class Occupation{

  Occupation({
    @required this.id,
    @required this.occupationCode,
    @required this.occupationName,
    @required this.displayLevel,
    @required this.selectable,
    @required this.sortSequence});

  final int id;
  final String occupationCode;
  final String occupationName;
  final int displayLevel;
  final String selectable;
  final int sortSequence;

  Map<String, dynamic> toMap() => {
    'id': id,
    'occupation_name': occupationName,
    'occupation_code': occupationCode,
    'display_level': displayLevel,
    'selectable': selectable,
    'sort_sequence': sortSequence,
  };

  factory Occupation.fromMap(Map<String, dynamic> map) => Occupation(
    id: map['id'],
    occupationName: map['occupation_name'],
    occupationCode: map['occupation_code'],
    displayLevel: map['display_level'],
    selectable: map['selectable'],
    sortSequence: map['sort_sequence'],
  );
//  "id INTEGER PRIMARY KEY,"
//  "occupation_code TEXT,"
//  "name TEXT,"
//  "display_level TEXT,"
//  "sort_sequence INTEGER,"
//  ")
}