import 'package:flutter/material.dart';
import 'package:meta/meta.dart';


class BasicSettings {
  BasicSettings({
    @required this.userId,
    @required this.name,
    @required this.notificationsSetting,
    @required this.incognitoSetting,
  });

  final String userId;
  final String name;
  final bool notificationsSetting;
  final bool incognitoSetting;

  Map<String, dynamic> toMap() => {
    'userId': userId,
    'name': name,
    'notificationSetting': notificationsSetting,
    'incognitoSetting': incognitoSetting,
  };

  factory BasicSettings.fromMap(Map<String, dynamic> map) => BasicSettings(
    userId: map['userId'],
    name: map['name'],
    notificationsSetting: map['notificationsSetting'],
    incognitoSetting: map['incognitoSetting'],
  );
}