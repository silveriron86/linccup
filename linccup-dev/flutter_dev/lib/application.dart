import 'package:flutter/material.dart';
import 'package:flutter_dev/database/db_provider.dart';
import 'package:provider/provider.dart';

import './firebase/apis.dart';
import './firebase/firebase_bloc.dart';
import './routes.dart';

Map<int, Color> color =
{
  50:Color.fromRGBO(97,0,121, .1),
  100:Color.fromRGBO(97,0,121, .2),
  200:Color.fromRGBO(97,0,121, .3),
  300:Color.fromRGBO(97,0,121, .4),
  400:Color.fromRGBO(97,0,121, .5),
  500:Color.fromRGBO(97,0,121, .6),
  600:Color.fromRGBO(97,0,121, .7),
  700:Color.fromRGBO(97,0,121, .8),
  800:Color.fromRGBO(97,0,121, .9),
  900:Color.fromRGBO(97,0,121, 1),
};

class Application extends StatelessWidget{
  Application(){
    DbProvider().createDatabase();
  }  

  @override
  Widget build(BuildContext context){
    return Provider<DbProvider>(
      builder: (_) => DbProvider(),
      dispose: (_, value) => value.dispose(),
      child: Provider<FirebaseBloc>(
        builder: (_)=> FirebaseBloc(apis: Apis()),
        dispose: (_, value) => value.dispose(),
        child: MaterialApp(
          title: 'Linccup',
          theme: ThemeData(
            primarySwatch: MaterialColor(0xFF6100ed, color),
          ),
          initialRoute: '/contacts',
          routes: routes,
          navigatorObservers: [routeObserver],
        ),
      ),
    );
  }

}