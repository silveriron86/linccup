import 'package:firebase_auth/firebase_auth.dart';

abstract class UserRepository {
  Future<bool> isAuthenticated();
  Future<void> authenticate();
  Future<String> getUserId();
  Future<void> signOut();
  Future<void> signUp();
  Future<FirebaseUser> getCurrentUser();
  Future<void> updateDisplayName(String displayName);
  Future<void> updatePhotoUrl(String photoUrl);
}
