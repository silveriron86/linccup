import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:equatable/equatable.dart';

class LinccupEntity extends Equatable {
  // https://developers.google.com/calendar/v3/reference/events
  final bool complete;
  final String id;
  final GeoPoint location;
  final String creatorId;
  final String summary;
  final List<dynamic> attendees;

  const LinccupEntity(
    this.complete,
    this.id,
    this.location,
    this.creatorId,
    this.summary,
    this.attendees,
  );

  Map<String, Object> toJson() {
    return {
      "complete": complete,
      "location": location,
      "creatorId": creatorId,
      "id": id,
      "summary": summary,
      "attendees": attendees,
    };
  }

  @override
  List<Object> get props =>
      [complete, id, creatorId, attendees, summary, location];

  @override
  String toString() {
    return 'LinccupEntity { complete: $complete, location: $location, summary: $summary, id: $id, creatorId: $creatorId, attendees: ${attendees.toString()} }';
  }

  static LinccupEntity fromJson(Map<String, Object> json) {
    return LinccupEntity(
      json["complete"] as bool,
      json["id"] as String,
      json["location"] as GeoPoint,
      json["creator_id"] as String,
      json["summary"] as String,
      json["attendees"] as List<dynamic>,
    );
  }

  static LinccupEntity fromSnapshot(DocumentSnapshot snap) {
    return LinccupEntity(
      snap.data['complete'],
      snap.documentID,
      snap.data['location'],
      snap.data['creator_id'],
      snap.data['summary'],
      snap.data['attendees'],
    );
  }

  Map<String, Object> toDocument() {
    return {
      "complete": complete,
      "location": location,
      "creatorId": creatorId,
      "summary": summary,
      "attendees": attendees,
    };
  }
}
