import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:linccups_repository/linccups_repository.dart';
import 'entities/entities.dart';

class FirebaseLinccupRepository implements LinccupsRepository {
  final linccupCollection = Firestore.instance.collection('linccups');

  @override
  Future<void> addNewLinccup(Linccup linccup) {
    return linccupCollection.add(linccup.toEntity().toDocument());
  }

  @override
  Future<void> deleteLinccup(Linccup linccup) {
    return linccupCollection.document(linccup.id).delete();
  }

  @override
  Stream<List<Linccup>> linccups() {
    return linccupCollection.snapshots().map((snapshot) {
      return snapshot.documents
          .map((doc) => Linccup.fromEntity(LinccupEntity.fromSnapshot(doc)))
          .toList();
    });
  }

  @override
  Future<void> updateLinccup(Linccup update) {
    return linccupCollection
        .document(update.id)
        .updateData(update.toEntity().toDocument());
  }
}
