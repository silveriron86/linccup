import 'dart:async';
import 'package:linccups_repository/linccups_repository.dart';

abstract class LinccupsRepository {
  Future<void> addNewLinccup(Linccup linccup);

  Future<void> deleteLinccup(Linccup linccup);

  Stream<List<Linccup>> linccups();

  Future<void> updateLinccup(Linccup linccup);
}
