import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:meta/meta.dart';

import '../entities/linccup_entity.dart';

@immutable
class Linccup {
  final bool complete;
  final String id;
  final GeoPoint location;
  final String address;
  final String type;
  final String fromDate;
  final String fromTime;
  final String toDate;
  final String toTime;
  final String creatorId;
  final String summary;
  final List<dynamic> attendees;

  Linccup(
    this.creatorId, {
    this.complete = false,
    GeoPoint location,
    String address,
    String type,
    String fromDate,
    String fromTime,
    String toDate,
    String toTime,
    String id,
    String summary,
    List<dynamic> attendees,
  })  : this.attendees = [creatorId],
        this.location = location ?? GeoPoint(0, 0),
        this.address = address ?? '',
        this.type = type ?? 'NONE',
        this.fromDate = fromDate ?? '',
        this.fromTime = fromTime ?? '',
        this.toDate = toDate ?? '',
        this.toTime = toTime ?? '',
        this.summary = summary ?? '',
        this.id = id;

  Linccup copyWith({
    bool complete,
    String id,
    GeoPoint location,
    String address,
    String type,
    String fromDate,
    String fromTime,
    String toDate,
    String toTime,
    String creatorId,
    String summary,
    List<dynamic> attendees,
  }) {
    return Linccup(
      creatorId ?? this.creatorId,
      complete: complete ?? this.complete,
      id: id ?? this.id,
      location: location ?? this.location,
      address: address ?? this.address,
      type: type ?? this.type,
      fromDate: fromDate ?? this.fromDate,
      fromTime: fromTime ?? this.fromTime,
      toDate: toDate ?? this.toDate,
      toTime: toTime ?? this.toTime,
      summary: summary ?? this.summary,
      attendees: attendees ?? this.attendees,
    );
  }

  factory Linccup.fromJson(Map<String, dynamic> json) => Linccup(
        'me',
        id: json["id"],
        address: json["location"],
        type: json["type"],
        fromDate: json["fromDate"],
        fromTime: json["fromTime"],
        toDate: json["toDate"],
        toTime: json["toTime"],
      );

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Linccup &&
          runtimeType == other.runtimeType &&
          complete == other.complete &&
          location == other.location &&
          creatorId == other.creatorId &&
          summary == other.summary &&
          attendees == other.attendees;

  @override
  String toString() {
    return 'LinccupEntity { complete: $complete, location: $location, summary: $summary, id: $id, creatorId: $creatorId, attendees: ${attendees.toString()} }';
  }

  LinccupEntity toEntity() {
    return LinccupEntity(
      complete,
      id,
      location,
      creatorId,
      summary,
      attendees,
    );
  }

  static Linccup fromEntity(LinccupEntity entity) {
    return Linccup(
      entity.creatorId,
      complete: entity.complete ?? false,
      location: entity.location,
      summary: entity.summary,
      attendees: entity.attendees,
      id: entity.id,
    );
  }
}
