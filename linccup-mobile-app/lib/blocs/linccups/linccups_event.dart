import 'package:equatable/equatable.dart';
import 'package:linccups_repository/linccups_repository.dart';

abstract class LinccupsEvent extends Equatable {
  const LinccupsEvent();

  @override
  List<Object> get props => [];
}

class LoadLinccups extends LinccupsEvent {}

class AddLinccup extends LinccupsEvent {
  final Linccup linccup;

  const AddLinccup(this.linccup);

  @override
  List<Object> get props => [linccup];

  @override
  String toString() => 'AddLinccup { linccup: $linccup }';
}

class UpdateLinccup extends LinccupsEvent {
  final Linccup updatedLinccup;

  const UpdateLinccup(this.updatedLinccup);

  @override
  List<Object> get props => [updatedLinccup];

  @override
  String toString() => 'UpdateLinccup { updatedLinccup: $updatedLinccup }';
}

class DeleteLinccup extends LinccupsEvent {
  final Linccup linccup;

  const DeleteLinccup(this.linccup);

  @override
  List<Object> get props => [linccup];

  @override
  String toString() => 'DeleteLinccup { linccup: $linccup }';
}

class ClearCompleted extends LinccupsEvent {}

class ToggleAll extends LinccupsEvent {}

class LinccupsUpdated extends LinccupsEvent {
  final List<Linccup> linccups;

  const LinccupsUpdated(this.linccups);

  @override
  List<Object> get props => [linccups];
}
