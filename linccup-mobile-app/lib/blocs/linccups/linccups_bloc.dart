import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:Linccup/blocs/linccups/linccups.dart';
import 'package:linccups_repository/linccups_repository.dart';

class LinccupsBloc extends Bloc<LinccupsEvent, LinccupsState> {
  final LinccupsRepository _linccupsRepository;
  StreamSubscription _linccupsSubscription;

  LinccupsBloc({@required LinccupsRepository linccupsRepository})
      : assert(linccupsRepository != null),
        _linccupsRepository = linccupsRepository;

  @override
  LinccupsState get initialState => LinccupsLoading();

  @override
  Stream<LinccupsState> mapEventToState(LinccupsEvent event) async* {
    if (event is LoadLinccups) {
      yield* _mapLoadLinccupsToState();
    } else if (event is AddLinccup) {
      yield* _mapAddLinccupToState(event);
    } else if (event is UpdateLinccup) {
      yield* _mapUpdateLinccupToState(event);
    } else if (event is DeleteLinccup) {
      yield* _mapDeleteLinccupToState(event);
    } else if (event is ToggleAll) {
      yield* _mapToggleAllToState();
    } else if (event is ClearCompleted) {
      yield* _mapClearCompletedToState();
    } else if (event is LinccupsUpdated) {
      yield* _mapLinccupsUpdateToState(event);
    }
  }

  Stream<LinccupsState> _mapLoadLinccupsToState() async* {
    _linccupsSubscription?.cancel();
    _linccupsSubscription = _linccupsRepository.linccups().listen(
          (linccups) => add(LinccupsUpdated(linccups)),
        );
  }

  Stream<LinccupsState> _mapAddLinccupToState(AddLinccup event) async* {
    _linccupsRepository.addNewLinccup(event.linccup);
  }

  Stream<LinccupsState> _mapUpdateLinccupToState(UpdateLinccup event) async* {
    _linccupsRepository.updateLinccup(event.updatedLinccup);
  }

  Stream<LinccupsState> _mapDeleteLinccupToState(DeleteLinccup event) async* {
    _linccupsRepository.deleteLinccup(event.linccup);
  }

  Stream<LinccupsState> _mapToggleAllToState() async* {
    final currentState = state;
    if (currentState is LinccupsLoaded) {
      final allComplete =
          currentState.linccups.every((linccup) => linccup.complete);
      final List<Linccup> updatedLinccups = currentState.linccups
          .map((linccup) => linccup.copyWith(complete: !allComplete))
          .toList();
      updatedLinccups.forEach((updatedLinccup) {
        _linccupsRepository.updateLinccup(updatedLinccup);
      });
    }
  }

  Stream<LinccupsState> _mapClearCompletedToState() async* {
    final currentState = state;
    if (currentState is LinccupsLoaded) {
      final List<Linccup> completedLinccups =
          currentState.linccups.where((linccup) => linccup.complete).toList();
      completedLinccups.forEach((completedLinccup) {
        _linccupsRepository.deleteLinccup(completedLinccup);
      });
    }
  }

  Stream<LinccupsState> _mapLinccupsUpdateToState(
      LinccupsUpdated event) async* {
    yield LinccupsLoaded(event.linccups);
  }

  @override
  Future<void> close() {
    _linccupsSubscription?.cancel();
    return super.close();
  }
}
