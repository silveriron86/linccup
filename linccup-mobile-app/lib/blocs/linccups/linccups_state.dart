import 'package:equatable/equatable.dart';
import 'package:linccups_repository/linccups_repository.dart';

abstract class LinccupsState extends Equatable {
  const LinccupsState();

  @override
  List<Object> get props => [];
}

class LinccupsLoading extends LinccupsState {}

class LinccupsLoaded extends LinccupsState {
  final List<Linccup> linccups;

  const LinccupsLoaded([this.linccups = const []]);

  @override
  List<Object> get props => [linccups];

  @override
  String toString() => 'LinccupsLoaded { linccups: $linccups }';
}

class LinccupsNotLoaded extends LinccupsState {}
