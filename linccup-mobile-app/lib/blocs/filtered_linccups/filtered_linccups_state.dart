import 'package:equatable/equatable.dart';

import 'package:linccups_repository/linccups_repository.dart';
import 'package:Linccup/models/models.dart';

abstract class FilteredLinccupsState extends Equatable {
  const FilteredLinccupsState();

  @override
  List<Object> get props => [];
}

class FilteredLinccupsLoading extends FilteredLinccupsState {}

class FilteredLinccupsLoaded extends FilteredLinccupsState {
  final List<Linccup> filteredLinccups;
  final VisibilityFilter activeFilter;

  const FilteredLinccupsLoaded(
    this.filteredLinccups,
    this.activeFilter,
  );

  @override
  List<Object> get props => [filteredLinccups, activeFilter];

  @override
  String toString() {
    return 'FilteredLinccupsLoaded { filteredLinccups: $filteredLinccups, activeFilter: $activeFilter }';
  }
}
