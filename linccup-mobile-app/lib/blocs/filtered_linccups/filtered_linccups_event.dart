import 'package:equatable/equatable.dart';
import 'package:linccups_repository/linccups_repository.dart';
import 'package:Linccup/models/models.dart';

abstract class FilteredLinccupsEvent extends Equatable {
  const FilteredLinccupsEvent();
}

class UpdateFilter extends FilteredLinccupsEvent {
  final VisibilityFilter filter;

  const UpdateFilter(this.filter);

  @override
  List<Object> get props => [filter];

  @override
  String toString() => 'UpdateFilter { filter: $filter }';
}

class UpdateLinccups extends FilteredLinccupsEvent {
  final List<Linccup> linccups;

  const UpdateLinccups(this.linccups);

  @override
  List<Object> get props => [linccups];

  @override
  String toString() => 'UpdateLinccups { linccups: $linccups }';
}
