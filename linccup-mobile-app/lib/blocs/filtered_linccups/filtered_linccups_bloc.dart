import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:linccups_repository/linccups_repository.dart';
import 'package:Linccup/blocs/filtered_linccups/filtered_linccups.dart';
import 'package:Linccup/blocs/linccups/linccups.dart';
import 'package:Linccup/models/models.dart';

import 'filtered_linccups.dart';

class FilteredLinccupsBloc
    extends Bloc<FilteredLinccupsEvent, FilteredLinccupsState> {
  final LinccupsBloc _linccupsBloc;
  StreamSubscription _linccupsSubscription;

  FilteredLinccupsBloc({@required LinccupsBloc linccupsBloc})
      : assert(linccupsBloc != null),
        _linccupsBloc = linccupsBloc {
    _linccupsSubscription = linccupsBloc.listen((state) {
      if (state is LinccupsLoaded) {
        add(UpdateLinccups((linccupsBloc.state as LinccupsLoaded).linccups));
      }
    });
  }

  @override
  FilteredLinccupsState get initialState {
    final currentState = _linccupsBloc.state;
    return currentState is LinccupsLoaded
        ? FilteredLinccupsLoaded(currentState.linccups, VisibilityFilter.all)
        : FilteredLinccupsLoading();
  }

  @override
  Stream<FilteredLinccupsState> mapEventToState(
      FilteredLinccupsEvent event) async* {
    if (event is UpdateFilter) {
      yield* _mapUpdateFilterToState(event);
    } else if (event is UpdateLinccups) {
      yield* _mapLinccupsUpdatedToState(event);
    }
  }

  Stream<FilteredLinccupsState> _mapUpdateFilterToState(
    UpdateFilter event,
  ) async* {
    final currentState = _linccupsBloc.state;
    if (currentState is LinccupsLoaded) {
      yield FilteredLinccupsLoaded(
        _mapLinccupsToFilteredLinccups(currentState.linccups, event.filter),
        event.filter,
      );
    }
  }

  Stream<FilteredLinccupsState> _mapLinccupsUpdatedToState(
    UpdateLinccups event,
  ) async* {
    final visibilityFilter = state is FilteredLinccupsLoaded
        ? (state as FilteredLinccupsLoaded).activeFilter
        : VisibilityFilter.all;
    yield FilteredLinccupsLoaded(
      _mapLinccupsToFilteredLinccups(
        (_linccupsBloc.state as LinccupsLoaded).linccups,
        visibilityFilter,
      ),
      visibilityFilter,
    );
  }

  List<Linccup> _mapLinccupsToFilteredLinccups(
      List<Linccup> linccups, VisibilityFilter filter) {
    return linccups.where((linccup) {
      if (filter == VisibilityFilter.all) {
        return true;
      } else if (filter == VisibilityFilter.active) {
        return !linccup.complete;
      } else {
        return linccup.complete;
      }
    }).toList();
  }

  @override
  Future<void> close() {
    _linccupsSubscription?.cancel();
    return super.close();
  }
}
