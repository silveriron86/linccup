import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:Linccup/blocs/tab/tab.dart';
import 'package:Linccup/models/models.dart';

class TabBloc extends Bloc<TabEvent, AppTab> {
  @override
  AppTab get initialState => AppTab.linccups;

  @override
  Stream<AppTab> mapEventToState(TabEvent event) async* {
    if (event is UpdateTab) {
      yield event.tab;
    }
  }
}
