import 'package:equatable/equatable.dart';

abstract class NotificationEvent extends Equatable {
  const NotificationEvent();

  @override
  List<Object> get props => [];
}

// events receive, view, dismiss, remove, snooze

class NotificationReceived extends NotificationEvent {}

class NotificationViewed extends NotificationEvent {}

class NotificationDismissed extends NotificationEvent {}

class NotificationRemoved extends NotificationEvent {}

class NotificationSnoozed extends NotificationEvent {}
