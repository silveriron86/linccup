import 'package:user_repository/user_repository.dart';
import 'package:flutter/material.dart';
import 'package:Linccup/common/constants.dart' as Constants;

import 'forgot_password_screen.dart';

class ForgotPasswordButton extends StatelessWidget {
  final FirebaseUserRepository _userRepository;

  ForgotPasswordButton({Key key, @required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      child: Text(
        Constants.FORGOT_PASSWORD,
      ),
      onPressed: () {
        Navigator.of(context).push(
          MaterialPageRoute(builder: (context) {
            return ForgotPasswordScreen(userRepository: _userRepository);
          }),
        );
      },
    );
  }
}
