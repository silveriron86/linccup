import 'dart:async';
import 'package:Linccup/forgot/bloc/forgot_state.dart';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:rxdart/rxdart.dart';

import 'package:user_repository/user_repository.dart';
import '../../validators.dart';
import 'forgot_event.dart';

class ForgotBloc extends Bloc<ForgotEvent, ForgotState> {
  FirebaseUserRepository _userRepository;

  ForgotBloc({
    @required UserRepository userRepository,
  })  : assert(userRepository != null),
        _userRepository = userRepository;

  @override
  ForgotState get initialState => ForgotState.empty();

  @override
  Stream<Transition<ForgotEvent, ForgotState>> transformEvents(
    Stream<ForgotEvent> events,
    TransitionFunction<ForgotEvent, ForgotState> next,
  ) {
    final nonDebounceStream = events.where((event) {
      return (event is! EmailChanged);
    });
    final debounceStream = events.where((event) {
      return (event is EmailChanged);
    }).debounceTime(Duration(milliseconds: 300));
    return super.transformEvents(
      nonDebounceStream.mergeWith([debounceStream]),
      next,
    );
  }

  @override
  Stream<ForgotState> mapEventToState(ForgotEvent event) async* {
    if (event is EmailChanged) {
      yield* _mapEmailChangedToState(event.email);
    }  else if (event is RequestPasswordPressed) {
      yield* _mapRequestPasswordPressedToState(
        email: event.email
      );
    }
  }

  Stream<ForgotState> _mapEmailChangedToState(String email) async* {
    yield state.update(
      isEmailValid: Validators.isValidEmail(email),
    );
  }

  Stream<ForgotState> _mapRequestPasswordPressedToState({
    String email,
  }) async* {
    yield ForgotState.loading();
    try {
      await _userRepository.sendPasswordResetEmail(email);
      yield ForgotState.success();
    } catch (_) {
      yield ForgotState.failure();
    }
  }
}
