import 'package:Linccup/forgot/bloc/bloc.dart';
import 'package:Linccup/login/create_account_button.dart';
import 'package:flutter/material.dart';
import 'package:Linccup/common/constants.dart' as Constants;
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:user_repository/user_repository.dart';

class ForgotPasswordForm extends StatefulWidget {
  final FirebaseUserRepository _userRepository;

  ForgotPasswordForm({Key key, @required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(key: key);
        
  @override
  _ForgotPasswordFormState createState() => _ForgotPasswordFormState();
}

class _ForgotPasswordFormState extends State<ForgotPasswordForm> {
  final TextEditingController _emailController = TextEditingController();

  ForgotBloc _forgotBloc;
  UserRepository get _userRepository => widget._userRepository;

  bool get isPopulated =>
      _emailController.text.isNotEmpty;

  bool isRequestForgotButtonEnabled(ForgotState state) {
    return state.isFormValid && isPopulated && !state.isSubmitting;
  }

  @override
  void initState() {
    super.initState();
    _forgotBloc = BlocProvider.of<ForgotBloc>(context);
    _emailController.addListener(_onEmailChanged);
  }

  @override
  void dispose() {
    _emailController.dispose();
    super.dispose();
  }

  void _onEmailChanged() {
    _forgotBloc.add(
      EmailChanged(email: _emailController.text),
    );
  }  

  @override
  Widget build(BuildContext context) {
    return BlocListener<ForgotBloc, ForgotState>(
      listener: (context, state) {
        if (state.isFailure) {
          // Failed
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [Text(Constants.REQUEST_FAILURE), Icon(Icons.error)],
                ),
                backgroundColor: Colors.red,
              ),
            );          
        }
        if (state.isSubmitting) {
          // submitting
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(Constants.REQUESTING),
                    CircularProgressIndicator(),
                  ],
                ),
              ),
            );          
        }
        if (state.isSuccess) {
          // success
          showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: null,
                content: Text(Constants.FORGOT_CONFIRM_MSG),
                actions: <Widget>[
                  FlatButton(
                    onPressed: () {
                      // Close alert dialog
                      Navigator.pop(context);
                      // navigates to Login after submission
                      Navigator.pop(context);
                    },
                    child: Text(Constants.OK)
                  )
                ],
              );
            }
          );           
        }
      },
      child: BlocBuilder<ForgotBloc, ForgotState>(
        builder: (context, state) {
          return Container(
            padding: EdgeInsets.all(20.0),
            child: Column(
              children: [
                Expanded(
                  child: Container(
                    padding: EdgeInsets.only(top: 30),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(height: 10),
                        Expanded(
                          child: Image.asset(
                            'assets/linccup-logo.png',
                            // height: ,
                            // color: Theme.of(context).primaryColor,
                          )
                        ),
                        SizedBox(height: 10),
                        Text(Constants.LINCCUP.toUpperCase(), style: TextStyle(fontWeight: FontWeight.bold, color: Color(0xFF2C2C2C), fontSize: 30)),
                      ]
                    )
                  )
                ),
                Expanded(
                  child: Container(
                    child: Form(
                      child: ListView(
                        children: <Widget>[
                          Text(Constants.ENTER_EMAIL, style: TextStyle(fontStyle: FontStyle.italic, color: Color(0xFF606060), fontSize: 19)),
                          SizedBox(height: 15),
                          TextFormField(
                            controller: _emailController,
                            decoration: InputDecoration(
                              hintText: Constants.CONTACT_EMAIL,
                              focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Color(0xFF666666))),
                              enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Color(0xFF666666))),
                            ),
                            keyboardType: TextInputType.emailAddress,
                            autovalidate: true,
                            autocorrect: false,
                            validator: (_) {
                              return !state.isEmailValid ? Constants.INVALID_EMAIL : null;
                            },
                          )
                        ]
                      )
                    )
                  )
                ),
                Expanded(
                  child: SizedBox(
                    child: Container(
                      padding: EdgeInsets.only(top: 20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          RaisedButton(
                            color: Theme.of(context).primaryColor,
                            onPressed: isRequestForgotButtonEnabled(state)
                                ? _onFormSubmitted
                                : null,
                            child: Text(Constants.REQUEST_PASSWORD, style: TextStyle(color: Colors.white)),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              FlatButton(
                                child: Text(Constants.SIGN_IN),
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                              ),
                              CreateAccountButton(userRepository: _userRepository)
                            ]
                          )
                        ],
                      )
                    )
                  )
                ),
              ]
            )
          );
        }
      )
    );
  }

  void _onFormSubmitted() {
    _forgotBloc.add(
      RequestPasswordPressed(
        email: _emailController.text
      ),
    );
  }  
}
