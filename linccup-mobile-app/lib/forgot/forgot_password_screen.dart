import 'package:Linccup/forgot/bloc/bloc.dart';
import 'package:Linccup/forgot/forgot_password_form.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:user_repository/user_repository.dart';

class ForgotPasswordScreen extends StatelessWidget {
  final FirebaseUserRepository _userRepository;

  ForgotPasswordScreen({Key key, @required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider<ForgotBloc>(
        create: (context) => ForgotBloc(userRepository: _userRepository),
        child: ForgotPasswordForm(userRepository: _userRepository),
      ),
    );
  }
}