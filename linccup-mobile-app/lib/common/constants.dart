library constants;

const String LINCCUPS = "Linccups";
const String LINCCUP = "Linccup";
const String GET_CONTACTS = "Get Contacts";

const String INVITE_FROM_CONTACTS = "Invite from Contacts";
const String SAVE = "SAVE";
const String LOCATION = "Location";
const String FROM = "From";
const String TO = "To";
const String MESSAGE = "Message";
const String ENTER_MESSAGE = "Enter a message";

const String SETTINGS = "Settings";
const String BASIC = "Basic";
const String NOTIFICATIONS = "Notifications";
const String INCOGNITO_MODE = "Incognito Mode";
const String DISPLAY_NAME = "Display Name";
const String CREATE_ACCOUNT = "Create an Account";
const String FORGOT_PASSWORD = "Forgot Password?";
const String SIGN_UP = "Sign Up";
const String SIGN_IN = "Sign In";
const String REQUEST_PASSWORD = "Request password";
const String ENTER_EMAIL = "Please Enter your Email Below";
const String FORGOT_CONFIRM_MSG =
    "If you submitted a valid email, you will receive a message from us soon with reset instructions included";
const String OK = "OK";
const String REQUEST_FAILURE = "Request Failure";
const String REQUESTING = "Sending Request...";

const String SIGN_GOOGLE = "Sign in with Google";
const String LOGIN = "Log In";
const String LOGIN_FAILURE = "Login Failure";
const String LOGGING_IN = "Logging In...";
const String EMAIL = "Email";
const String CONTACT_EMAIL = "Contact Email";
const String INVALID_EMAIL = "Invalid Email";
const String PASSWORD = "Password";
const String INVALID_PASSWORD = "Invalid Password";
const String LOGIN_LINCCUP = "Login to Linccup";

const String REGISTER = "Register";

const String REGISTERING = "Registering...";
const String REGISTRATION_FAILURE = "Registration Failure";
const String REGISTER_LINCCUP = "Register for Linccup";
const String CONTACTS_NEARME = "Contacts Near Me";
const String NEW_LINCCUPS = "New Linccup";
const String GRAB_SOMEFOOD = "Grab some food";
const String MEET_COFFEE = "Meet for coffee";
const String MEET_CHAT = "General Chat";
const String SPLASH_SCREEN = "Splash Screen";
const String UNDO = "Undo";
const String DELETED = "Deleted";
const String INVITED = "Invited";
const String DECLINED = "Declined";

const String WELCOME_TO = "Welcome To";
const String UPLOAD_PROFILE_PHOTO = "Upload a Profile Photo";
const String ONLY_SHARED_CONTACTS = "Only shared with Contacts";
const String FINISH = "Finish";
const String ADD = "Add";
