import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart';
import 'package:Linccup/hive/hive.dart';
import 'package:user_repository/user_repository.dart';
import 'package:Linccup/common/constants.dart' as Constants;

class SettingsScreen extends StatefulWidget {
  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen>  {
  final TextEditingController nameController = TextEditingController();
  final UserRepository userRepository = FirebaseUserRepository();

  Box _settingsBox;
  bool notifications = false;
  bool incognitoMode = false;
  
  @override
  void initState() {
    super.initState();
    _getDisplayName();
    _openBox();
  }

  Future _getDisplayName() async {
    FirebaseUser user =  await userRepository.getCurrentUser();
    nameController.text = user.displayName;
    return;
  }

  _updateDisplayName(String value) {
    userRepository.updateDisplayName(value);
  }

  Future _openBox() async {
    var dir = await getApplicationDocumentsDirectory();
    Hive.init(dir.path);
    _settingsBox = await Hive.openBox('settingsBox');
    if (_settingsBox.isEmpty == true) {
      _settingsBox.add(SettingsModel(false, false));
    } else {
      SettingsModel settingsModel = _settingsBox.values.toList()[0];
      setState(() {
        notifications = settingsModel.notifications;
        incognitoMode = settingsModel.incognitoMode;
      });
    }
    return;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(Constants.SETTINGS),
      ),
      body: Form(
        child: Container(
          height: 340,
          padding: const EdgeInsets.only(
            left: 20.0,
            top: 50.0,
            right: 12.0,
          ),
          decoration: BoxDecoration(
            border: Border(bottom: BorderSide(color: Theme.of(context).primaryColorDark, width: 3))
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(top: 20, bottom: 40),
                child: Text(Constants.BASIC, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18, color: Color(0xFF393939))),
              ),
              Container(
                margin: EdgeInsets.only(right: 8, bottom: 20),
                child: TextField(
                  controller: nameController,
                  decoration: InputDecoration(
                    fillColor: Color(0xFFEDEDED),
                    filled: true,
                    hintText: Constants.DISPLAY_NAME,
                    hintStyle: TextStyle(color: Color(0xFF5F5F5F))
                  ),
                  onSubmitted: (String value) {
                    _updateDisplayName(value);
                  },                  
                )
              ),
              SwitchListTile(
                contentPadding: EdgeInsets.only(left: 5, right: 0),
                title: const Text(Constants.NOTIFICATIONS),
                value: notifications,
                onChanged: (bool value) {
                  SettingsModel settingsModel = _settingsBox.values.toList()[0];
                  settingsModel.notifications = value;
                  _settingsBox.putAt(0, settingsModel);
                  setState(() {
                    notifications = value;
                  });
                },
                // secondary: const Icon(Icons.notifications),
              ),
              SwitchListTile(
                contentPadding: EdgeInsets.only(left: 5, right: 0),
                title: Text(Constants.INCOGNITO_MODE),
                value: incognitoMode,
                onChanged: (bool value) {
                  SettingsModel settingsModel = _settingsBox.values.toList()[0];
                  settingsModel.incognitoMode = value;
                  _settingsBox.putAt(0, settingsModel);
                  setState(() {
                    incognitoMode = value;
                  });                  
                },
                // secondary: const Icon(Icons.gps_off),
              ),
            ],
          )),
      ),
    );
  }
}
