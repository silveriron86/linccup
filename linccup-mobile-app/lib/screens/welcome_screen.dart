import 'dart:async';
import 'dart:io';

import 'package:Linccup/common/constants.dart' as Constants;
import 'package:Linccup/common/globals.dart' as globals;
import 'package:Linccup/widgets/loading_indicator.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart';
import 'package:user_repository/user_repository.dart';

class WelcomeScreen extends StatefulWidget {
  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  File _image;
  bool loading = false;
  final UserRepository userRepository = FirebaseUserRepository();

  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);

    setState(() {
      _image = image;
    });
  }

  void finish(context) {
    if (_image != null) {
      // upload _image
      setState(() {
        loading = true;
      });
      _uploadFile(context);
    } else {
      _goNext(context);
    }
  }

  Future<void> _uploadFile(context) async {
    String url =
        'photos/${(new DateTime.now().millisecondsSinceEpoch)}${basename(_image.path)}}';
    print(url);
    StorageReference storageReference =
        FirebaseStorage.instance.ref().child(url);
    StorageUploadTask uploadTask = storageReference.putFile(_image);
    await uploadTask.onComplete;
    storageReference.getDownloadURL().then((uploadedFileURL) {
      // update user's photo url
      userRepository.updatePhotoUrl(uploadedFileURL).then((value) => {
            setState(() {
              loading = false;
            }),
            _goNext(context)
          });
    });
  }

  _goNext(context) {
    globals.isRegistered = false;
    Navigator.pushNamed(context, '/home-screen');
  }

  @override
  Widget build(BuildContext context) {
    const titleStyle = TextStyle(
        fontWeight: FontWeight.bold, color: Color(0xFF2C2C2C), fontSize: 30);
    const labelStyle = TextStyle(
        fontStyle: FontStyle.italic, color: Color(0xFF3C3C3C), fontSize: 19);

    return Scaffold(
        body: Stack(children: [
      Container(
          child: Column(children: [
        Container(
            padding: EdgeInsets.only(top: 80, bottom: 45),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          'assets/linccup-logo.png',
                          height: 80,
                          // color: Theme.of(context).primaryColor,
                        ),
                        SizedBox(width: 15),
                        Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(Constants.WELCOME_TO.toUpperCase(),
                                  style: titleStyle),
                              Text(Constants.LINCCUP.toUpperCase(),
                                  style: titleStyle),
                            ])
                      ]),
                  SizedBox(height: 40),
                  Text(Constants.UPLOAD_PROFILE_PHOTO, style: labelStyle),
                  SizedBox(height: 10),
                  Text(Constants.ONLY_SHARED_CONTACTS, style: labelStyle),
                ])),
        Container(
            child: SizedBox(
          width: 200.0,
          height: 200.0,
          child: RaisedButton(
            padding: EdgeInsets.all(0),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(100.0),
            ),
            onPressed: getImage,
            child: ClipRRect(
                borderRadius: BorderRadius.circular(100.0),
                child: _image != null
                    ? Image.file(_image,
                        width: 200, height: 200, fit: BoxFit.fitWidth)
                    : Image.asset(
                        'assets/upload_avatar.png',
                        width: 200,
                        height: 200,
                        // color: Theme.of(context).primaryColor,
                      )),
          ),
        )),
        Expanded(
            child: Container(
          padding: EdgeInsets.all(20),
          child: Align(
              alignment: Alignment.bottomCenter,
              child: SizedBox(
                  width: double.infinity,
                  child: RaisedButton(
                      onPressed: () {
                        finish(context);
                      },
                      color: Theme.of(context).primaryColor,
                      child: Text(Constants.FINISH.toUpperCase(),
                          style: TextStyle(color: Colors.white))))),
        ))
      ])),
      loading == true ? LoadingIndicator() : Container()
    ]));
  }
}
