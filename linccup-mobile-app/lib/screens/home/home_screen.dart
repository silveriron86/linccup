import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:ui';

import 'package:Linccup/blocs/blocs.dart';
import 'package:Linccup/common/constants.dart' as Constants;
import 'package:Linccup/common/keys.dart' as Keys;
import 'package:Linccup/screens/home/contacts_list.dart';
import 'package:Linccup/screens/home/user_linccups.dart';
import 'package:Linccup/screens/linccup_invite_screen.dart';
import 'package:Linccup/widgets/widgets.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:contacts_service/contacts_service.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:linccups_repository/linccups_repository.dart';
import 'package:path/path.dart' as p;
import 'package:permission_handler/permission_handler.dart';
import 'package:user_repository/user_repository.dart';

const meetImages = [
  "assets/images/slides/87745526.jpg",
  "assets/images/slides/58705368.jpg",
  "assets/images/slides/283523149.jpg",
];
const meetTypes = [
  Constants.GRAB_SOMEFOOD,
  Constants.MEET_COFFEE,
  Constants.MEET_CHAT
];

class HomeScreen extends StatefulWidget {
  // final String name;

  // HomeScreen({Key key, @required this.name}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final databaseReference = Firestore.instance;
  final Firestore _db = Firestore.instance;
  final FirebaseMessaging _fcm = FirebaseMessaging();

  StreamSubscription iosSubscription;

  Future<PermissionStatus> permissionStatus;
  Future<Map<Permission, PermissionStatus>> statuses;
  Future<Position> gpsPosition;
  FirebaseUser user;
  List<dynamic> linccups = [];
  List<dynamic> linccupContacts = [];
  List<String> linccupUsersEmails = [];
  bool loading = true;
  bool loadingContacts = false;

  final UserRepository userRepository = FirebaseUserRepository();

  Future<Position> _getGPSPosition() async {
    Position position = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best);
    return position;
  }

  Future<Map<Permission, PermissionStatus>> _getPermissions() async {
    Map<Permission, PermissionStatus> statuses = await [
      Permission.location,
      Permission.contacts,
    ].request();

    if (statuses[Permission.contacts].isGranted) {
      print("Contacts Permission Granted");
    }

    if (statuses[Permission.location].isGranted) {
      print("Location Permission Granted");
    }
    return statuses;
  }

  getCurrentUser() async {
    UserRepository userRepository = FirebaseUserRepository();
    FirebaseUser firebaseUser = await userRepository.getCurrentUser();
    setState(() {
      user = firebaseUser;
    });
  }

  Future getImage() async {
    var _image = await ImagePicker.pickImage(source: ImageSource.camera);
    String url =
        'photos/${(new DateTime.now().millisecondsSinceEpoch)}${p.basename(_image.path)}}';
    print(url);
    StorageReference storageReference =
        FirebaseStorage.instance.ref().child(url);
    StorageUploadTask uploadTask = storageReference.putFile(_image);
    await uploadTask.onComplete;
    storageReference.getDownloadURL().then((uploadedFileURL) {
      // update user's photo url
      userRepository
          .updatePhotoUrl(uploadedFileURL)
          .then((value) => {getCurrentUser()});
    });
  }

  @override
  void initState() {
    statuses = _getPermissions();
    gpsPosition = _getGPSPosition();
    getCurrentUser();
    _saveDeviceToken();
    if (Platform.isIOS) {
      iosSubscription = _fcm.onIosSettingsRegistered.listen((data) {
        //save the token or subscribe to a topic here
      });
      _fcm.requestNotificationPermissions(IosNotificationSettings());
    }

    _fcm.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        showDialog(
          context: context,
          builder: (context) => AlertDialog(
            content: ListTile(
              title: Text(message['notification']['title']),
              subtitle: Text(message['notification']['body']),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('Ok'),
                onPressed: () => Navigator.of(context).pop(),
              ),
            ],
          ),
        );
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
        // TODO optional
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        // TODO optional
      },
    );
    super.initState();
    _getAllLinccupUsers();
    _getLinccupUser();
  }

  Future<void> _getAllLinccupUsers() async {
    databaseReference
        .collection("linccupUsers")
        .getDocuments()
        .then((QuerySnapshot snapshot) {
      List<String> usersEmails = [];
      snapshot.documents
          .forEach((f) => {usersEmails.add(f.documentID.toUpperCase())});
      setState(() {
        linccupUsersEmails = usersEmails;
      });
    });
  }

  void _saveDeviceToken() async {
    // Get the current user
    FirebaseUser user = await userRepository.getCurrentUser();

    // Get the token for this device
    String fcmToken = await _fcm.getToken();

    // Save it to Firestore
    if (fcmToken != null) {
      var tokens = _db
          .collection('linccupUsers')
          .document(user.email)
          .collection('tokens')
          .document(fcmToken);

      await tokens.setData({
        'token': fcmToken,
        'createdAt': FieldValue.serverTimestamp(), // optional
        'platform': Platform.operatingSystem // optional
      });
    }
  }

  List<Linccup> linccupsFromJson(String str) => List<Linccup>.from(
      json.decode(str)["linccups"].map((x) => Linccup.fromJson(x)));

  Future<String> _getToken() async {
    UserRepository userRepository = FirebaseUserRepository();
    FirebaseUser user = await userRepository.getCurrentUser();
    IdTokenResult idToken = await user.getIdToken();
    print(idToken.token);
    return idToken.token;
  }

  Future<void> _getLinccupUser() async {
    final http.Response response = await http.post(
      '${Keys.baseApiURL}getLinccupUser',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'authToken': await _getToken(),
      }),
    );
    if (response.statusCode == 200) {
      Map<String, dynamic> data = json.decode(response.body);
      var isExistLinccups = data["linccups"];
      var isContacts = data["contacts"];
      setState(() {
        linccups =
            isExistLinccups != null ? linccupsFromJson(response.body) : [];
        linccupContacts = isContacts != null ? data['contacts'] : [];
        loading = false;
        loadingContacts = false;
      });
    } else {
      setState(() {
        loading = false;
        loadingContacts = false;
      });
      // throw Exception('Failed to load linccup');
    }
  }

  Future<void> addContact(Contact contact) async {
    setState(() {
      loadingContacts = true;
    });
    List<String> emails = (List<dynamic>.from(contact.emails))
        .map((e) => e.value as String)
        .toList();
    Map<String, dynamic> contactData = {
      "displayName": contact.displayName,
      "emails": emails,
      "accepted": false,
      "timestamp": (new DateTime.now().millisecondsSinceEpoch)
    };

    final http.Response response = await http.post(
      '${Keys.baseApiURL}addContact',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, dynamic>{
        'authToken': await _getToken(),
        'contact': contactData,
      }),
    );
    if (response.statusCode == 200) {
      print(response);
      _getLinccupUser();
    } else {
      throw Exception('Failed to load linccup');
    }
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider<TabBloc>(
            create: (context) => TabBloc(),
          ),
          BlocProvider<FilteredLinccupsBloc>(
            create: (context) => FilteredLinccupsBloc(
              linccupsBloc: BlocProvider.of<LinccupsBloc>(context),
            ),
          ),
        ],
        child: Scaffold(
            appBar: AppBar(
              title: Text(Constants.LINCCUPS),
              actions: <Widget>[
                IconButton(
                  icon: Icon(FontAwesomeIcons.slidersH, size: 17),
                  onPressed: () {
                    Navigator.pushNamed(context, '/settings-screen');
                  },
                ),
              ],
            ),
            drawer: Menu(
              firebaseUser: user,
              onGetImage: getImage,
            ),
            body: Stack(
              alignment: AlignmentDirectional.center,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width, //- 100,
                      height: 150,
                      padding: EdgeInsets.only(top: 10, bottom: 10),
                      decoration: BoxDecoration(
                        color: Colors.white,
                      ),
                      child: CarouselSlider(
                        options: CarouselOptions(
                          autoPlay: true,
                          enlargeCenterPage: true,
                          initialPage: 1,
                          viewportFraction: 0.9,
                        ),
                        items: meetImages
                            .map((img) => Container(
                                    child: Stack(
                                  children: <Widget>[
                                    ClipRRect(
                                        borderRadius:
                                            BorderRadius.circular(10.0),
                                        child: InkWell(
                                          onTap: () {
                                            Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                builder: (context) =>
                                                    LinccupInviteScreen(
                                                        meet: meetTypes[
                                                            meetImages
                                                                .indexOf(img)],
                                                        refresh:
                                                            _getLinccupUser),
                                              ),
                                            );
                                          },
                                          child: Image.asset(
                                            img,
                                            fit: BoxFit.cover,
                                            width: (MediaQuery.of(context)
                                                        .size
                                                        .width -
                                                    100) /
                                                1.1,
                                          ),
                                        )),
                                    Positioned(
                                      bottom: 10,
                                      right: 10,
                                      child: ClipRRect(
                                        borderRadius:
                                            BorderRadius.circular(4.0),
                                        child: Container(
                                          color: Colors.black.withOpacity(0.7),
                                          height: 28.0,
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 3.0),
                                          alignment: Alignment.bottomCenter,
                                          child: Text(
                                            meetTypes[meetImages.indexOf(img)]
                                                .toString(),
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 22.0,
                                              fontStyle: FontStyle.normal,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                )))
                            .toList(),
                      ),
                    ),
                    Container(
                        height: 30,
                        width: double.infinity,
                        margin: EdgeInsets.only(left: 20.0),
                        decoration: BoxDecoration(
                          color: Colors.white,
                        ),
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(Constants.LINCCUPS,
                                  style: TextStyle(
                                      color: Theme.of(context)
                                          .secondaryHeaderColor,
                                      fontSize: 16))
                            ])),
                    Expanded(
                      child: Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                          ),
                          child: UserLinccups(
                              linccups: linccups, loading: loading)),
                    ),
                    Container(
                        height: 30,
                        width: double.infinity,
                        padding: EdgeInsets.only(left: 15.0),
                        decoration: BoxDecoration(
                          color: Theme.of(context).primaryColorLight,
                        ),
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(Constants.CONTACTS_NEARME,
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 18))
                            ])),
                    Expanded(
                      child: Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                          ),
                          child: ContactsList(
                              linccupContacts: linccupContacts,
                              linccupUsersEmails: linccupUsersEmails,
                              loading: loadingContacts,
                              addContact: addContact)),
                    ),
                  ],
                ),
                Container(
                    child: FutureBuilder<Map<Permission, PermissionStatus>>(
                  future: statuses,
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return Container();
                    } else if (snapshot.hasError) {
                      return Container();
                    }
                    return LoadingIndicator();
                  },
                ))
              ],
            )));
  }
}

class GetLocation extends StatefulWidget {
  final Future<Position> gpsPosition;
  GetLocation({@required this.gpsPosition});
  @override
  _GetLocationState createState() => _GetLocationState();
}

class _GetLocationState extends State<GetLocation> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: FutureBuilder<Position>(
        future: widget.gpsPosition,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Text(
                'lat: ${snapshot.data.latitude} - lon: ${snapshot.data.longitude}');
          } else if (snapshot.hasError) {
            return Text('location Error: ${snapshot.error}');
          } else {
            return LoadingIndicator();
          }
        },
      ),
    );
  }
}

class GetContacts extends StatefulWidget {
  @override
  _GetContactsState createState() => _GetContactsState();
}

class _GetContactsState extends State<GetContacts> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text(Constants.GET_CONTACTS),
    );
  }
}
