import 'package:Linccup/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class UserLinccups extends StatefulWidget {
  final List<dynamic> linccups;
  final bool loading;
  UserLinccups({Key key, @required this.linccups, @required this.loading})
      : super(key: key);

  @override
  _UserLinccupsState createState() => _UserLinccupsState();
}

class _UserLinccupsState extends State<UserLinccups> {
  _UserLinccupsState();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget build(BuildContext context) {
    if (widget.loading == true) {
      return LoadingIndicator();
    } else {
      final linccups = widget.linccups;
      return Container(
        height: 600,
        child: ListView.builder(
          itemCount: linccups.length,
          itemBuilder: (context, index) {
            dynamic linccup = linccups[index];
            return LinccupItem(
              linccup: linccup,
              onDismissed: (direction) {},
              onTap: () async {},
              onCheckboxChanged: (bool value) {},
            );
          },
        ),
      );
    }
  }
}
