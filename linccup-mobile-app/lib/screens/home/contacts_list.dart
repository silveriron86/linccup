import 'package:Linccup/models/models.dart';
import 'package:Linccup/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:contacts_service/contacts_service.dart';
import 'package:Linccup/common/constants.dart' as Constants;

class ContactsList extends StatefulWidget {
  final List<dynamic> linccupContacts;
  final List<String> linccupUsersEmails;
  final bool loading;

  final Function addContact;
  ContactsList(
      {Key key,
      @required this.linccupContacts,
      @required this.linccupUsersEmails,
      @required this.loading,
      @required this.addContact})
      : super(key: key);

  @override
  _ContactsListState createState() => _ContactsListState();
}

class _ContactsListState extends State<ContactsList> {
  Future<Iterable<CustomContact>> _contacts;

  Future<Iterable<CustomContact>> _getContacts() async {
    var contacts = await ContactsService.getContacts();
    return _populateContacts(contacts);
  }

  @override
  void initState() {
    _contacts = _getContacts();
    super.initState();
  }

  Iterable<CustomContact> _populateContacts(Iterable<Contact> contacts) {
    List<CustomContact> _allContacts = List<CustomContact>();
    List<Contact> _contacts = new List<Contact>();
    _contacts = contacts
        .where((item) => item.displayName != null && item.emails.length > 0)
        .toList();
    _contacts.sort((a, b) => a.displayName.compareTo(b.displayName));
    _allContacts =
        _contacts.map((contact) => CustomContact(contact: contact)).toList();
    return _allContacts;
  }

  bool _checkIsAppUser(
      List<String> _linccupUsersEmails, CustomContact _contact) {
    if (_linccupUsersEmails.length > 0) {
      Contact contact = _contact.contact;
      List<dynamic> found = contact.emails.where((el) {
        bool isExists = contact.emails.any(
            (el) => _linccupUsersEmails.indexOf(el.value.toUpperCase()) > -1);
        return isExists;
      }).toList();

      if (found.length > 0) {
        return true;
      }
    }
    return false;
  }

  // check if the contact itme is in the linccup user's contacts
  bool _checkIsLinccupContact(
      List<dynamic> _linccupContacts, CustomContact _contact) {
    if (_linccupContacts.length > 0) {
      Contact contact = _contact.contact;
      if (contact.displayName != null) {
        List<dynamic> found = _linccupContacts.where((c) {
          List<String> emails = List<String>.from(
              c['emails'].map((e) => e.toUpperCase() as String));
          return contact.emails
              .any((el) => emails.indexOf(el.value.toUpperCase()) > -1);
        }).toList();

        if (found.length > 0) {
          return true;
        }
      }
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    return Stack(children: <Widget>[
      Container(
        child: FutureBuilder<Iterable<CustomContact>>(
            future: _contacts,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return ListView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (context, index) {
                    CustomContact contact = snapshot.data.elementAt(index);
                    return ContactItem(
                        item: contact,
                        isAppUser:
                            _checkIsAppUser(widget.linccupUsersEmails, contact),
                        isLinccup: _checkIsLinccupContact(
                            widget.linccupContacts, contact),
                        addContact: widget.addContact);
                  },
                );
              } else if (snapshot.hasError) {
                return Text("${snapshot.error}");
              }
              return LoadingIndicator();
            }),
      ),
      widget.loading == true ? LoadingIndicator() : Container()
    ]);
  }
}

class ContactItem extends StatefulWidget {
  final CustomContact item;
  final bool isLinccup;
  final bool isAppUser;
  final Function addContact;

  ContactItem(
      {Key key,
      @required this.item,
      @required this.isAppUser,
      @required this.isLinccup,
      @required this.addContact})
      : super(key: key);

  @override
  _ContactItemState createState() => _ContactItemState();
}

class _ContactItemState extends State<ContactItem> {
  CustomContact _contact;

  @override
  void initState() {
    _contact = widget.item;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Contact _item = this._contact.contact;
    bool _isLinccup = widget.isLinccup;
    bool _isAppUser = widget.isAppUser;
    return Container(
        child: ListTile(
      leading: Container(
        margin: EdgeInsets.only(left: 5),
        child: (_item.avatar != null)
            ? CircleAvatar(
                backgroundColor: Color(0xFFBDBDBD),
                backgroundImage: MemoryImage(_item.avatar))
            : CircleAvatar(
                backgroundColor: Color(0xFFBDBDBD),
                child: Text(
                    (_item.displayName[0] + _item.displayName[1]).toUpperCase(),
                    style: TextStyle(color: Colors.white)),
              ),
      ),
      title: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        Text(_item.displayName ?? ""),
        _isLinccup == false && _isAppUser == true
            ? SizedBox(
                width: 50,
                child: FlatButton(
                  padding: EdgeInsets.symmetric(vertical: 0, horizontal: 0),
                  onPressed: () {
                    widget.addContact(_item);
                  },
                  child: Text(Constants.ADD,
                      style: TextStyle(
                          color: Theme.of(context).primaryColor, fontSize: 16)),
                ))
            : Container()
      ]),
      subtitle: _isLinccup == false
          ? null
          : Align(
              alignment: Alignment.centerLeft,
              child: Container(
                // width: 100,
                padding: EdgeInsets.symmetric(vertical: 2, horizontal: 5),
                decoration:
                    BoxDecoration(color: Theme.of(context).primaryColor),
                child: Text(
                  'Linccup',
                  style: TextStyle(color: Colors.white),
                ),
              )),
      trailing: Checkbox(
          activeColor: Theme.of(context).primaryColorLight,
          value: _contact.isChecked,
          onChanged: (bool value) {
            setState(() {
              _contact.isChecked = value;
            });
          }),
    ));
  }
}
