import 'package:Linccup/widgets/widgets.dart';
import 'package:contacts_service/contacts_service.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:intl/intl.dart';
import 'package:user_repository/user_repository.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:Linccup/common/constants.dart' as Constants;
import 'package:Linccup/common/keys.dart' as Keys;

class LinccupInviteScreen extends StatefulWidget {
  final String meet;
  final Function refresh;
  LinccupInviteScreen({Key key, @required this.meet, @required this.refresh}) : super(key: key);

  @override
  _LinccupInviteScreenState createState() => _LinccupInviteScreenState();
}

class _LinccupInviteScreenState extends State<LinccupInviteScreen> {
  final locationController = TextEditingController();
  final messageController = TextEditingController();
  String meet;
  GlobalKey key = new GlobalKey<AutoCompleteTextFieldState<Contact>>();
  AutoCompleteTextField<Contact> contactsField;    
  DateTime fromDate = DateTime.now();
  DateTime toDate = DateTime.now();
  TimeOfDay fromTime = TimeOfDay.now();
  TimeOfDay toTime = TimeOfDay.now();
  List<Contact> _contacts = [];
  Contact selected;
  String location = '';
  bool loading = false;

  _onChanged(String value) {
    if(contactsField != null)
      contactsField.textField.controller.text = value;
  }

  _LinccupInviteScreenState() {
    contactsField = new AutoCompleteTextField<Contact>(
      decoration: new InputDecoration(
        border: InputBorder.none,
        hintText: Constants.INVITE_FROM_CONTACTS,
        hintStyle: TextStyle(fontSize: 24),
        suffixIcon: null
      ),
      style: TextStyle(fontSize: 24),
      itemSubmitted: (item) => _onChanged(item.displayName),
      key: key,
      suggestions: _contacts,
      itemBuilder: (context, suggestion) => new Padding(
          child: new ListTile(
              title: new Text(suggestion.displayName)),
          padding: EdgeInsets.all(8.0)),
      itemSorter: (a, b) => a.displayName.compareTo(b.displayName),
      itemFilter: (suggestion, input) =>
          (suggestion.displayName).toLowerCase().startsWith(input.toLowerCase()),
      clearOnSubmit: false,
    );
  }

  @override
  void initState() {
    super.initState();
    meet = widget.meet;
    // messageController.text = "Hi, I thought it would be fun to meet and talk about the new version of brewtex we discussed last month. Cheers!";
    ContactsService.getContacts().then((foundContacts) {
      List<Contact> contacts = new List<Contact>();
      contacts = foundContacts.where((item) => item.displayName != null && item.emails.length > 0).toList();
      setState(() {
        _contacts = contacts;
      });
      contactsField.updateSuggestions(contacts);
      _onChanged('');
    });
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    messageController.dispose();
    locationController.dispose();
    super.dispose();
  }

  DateTime _getDateTime(TimeOfDay tod) {
    final now = new DateTime.now();
    final dt = DateTime(now.year, now.month, now.day, tod.hour, tod.minute);
    return dt;
  }    

  Future<void> _createLinccup() async {
    setState((){
      loading = true;
    });

    UserRepository userRepository = FirebaseUserRepository();
    FirebaseUser user =  await userRepository.getCurrentUser();
    IdTokenResult idToken = await user.getIdToken();

    var dateFormatter = new DateFormat('yyyy-MM-dd');
    Map<String, dynamic> linccupData = {
      "displayName": contactsField.textField.controller.text,
      "type": meet,
      "fromDate": dateFormatter.format(fromDate),
      "fromTime": TimeOfDay.fromDateTime(_getDateTime(fromTime)).format(context),
      "toDate": dateFormatter.format(toDate),
      "toTime": TimeOfDay.fromDateTime(_getDateTime(toTime)).format(context),
      "location": location,
      "message": messageController.text,
      "status": "accepted"
    };

    final http.Response response = await http.post(
      '${Keys.baseApiURL}createLinccup',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, dynamic>{
        'authToken': idToken.token,
        'linccup': linccupData
      }),
    );
    if (response.statusCode == 200) {
      setState((){
        loading = false;
      });
      widget.refresh();
      Navigator.of(context).pop();      
    } else {
      setState((){
        loading = false;
      });
      throw Exception('Failed to load album');
    }
  }  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.close, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ), 
        title: Text(Constants.NEW_LINCCUPS),
        actions: <Widget>[
          FlatButton(
            onPressed: _createLinccup,
            child: Text(
              Constants.SAVE,
              style: TextStyle(color: Colors.white, fontSize: 16)
            ),
          )
        ],
      ),
      body: Stack(children: <Widget>[
        Container(
          decoration: BoxDecoration(
            color: Colors.white,
          ),
          child: ListView(
            children: <Widget>[
              Container(
                width: double.infinity,
                padding: EdgeInsets.only(left: 10, right: 10, top: 8, bottom: 8),
                decoration: BoxDecoration(
                  border: Border(bottom: BorderSide(color: Theme.of(context).primaryColorDark))
                ),
                child: DropdownButtonHideUnderline(
                  child: ButtonTheme(
                    alignedDropdown: true,
                    child: DropdownButton<String>(
                      // isExpanded: true,
                      items: [
                        DropdownMenuItem<String>(
                          child: Text(Constants.GRAB_SOMEFOOD),
                          value: Constants.GRAB_SOMEFOOD,
                        ),
                        DropdownMenuItem<String>(
                          child: Text(Constants.MEET_COFFEE),
                          value: Constants.MEET_COFFEE,
                        ),
                        DropdownMenuItem<String>(
                          child: Text(Constants.MEET_CHAT),
                          value: Constants.MEET_CHAT,
                        ),
                      ],
                      onChanged: (String value) {
                        setState(() {
                          meet = value;
                        });
                      },
                      hint: Text(Constants.MEET_COFFEE, style: TextStyle(color: Theme.of(context).primaryColorDark)),
                      value: meet,
                      style: TextStyle(fontSize: 25, color: Theme.of(context).primaryColorDark),
                      iconEnabledColor: Theme.of(context).primaryColorDark,
                      iconSize: 40,
                    )
                  )
                )
              ),
              Container(
                padding: EdgeInsets.only(top: 35, bottom: 45, left: 25, right: 25),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    contactsField,
                    Container(
                      width: double.infinity,
                      margin: EdgeInsets.only(top: 20, bottom: 30),
                      child: PlaceAutocomplete(onChange: (String v) => {
                        setState(() {
                          location = v;
                        })
                      })
                    ),
                    Text(Constants.FROM),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        DatePicker(value: fromDate, onChange: (DateTime v) => {
                          setState(() {
                            fromDate = v;
                          })
                        }),
                        TimePicker(value: fromTime, onChange: (TimeOfDay v) => {
                          setState(() {
                            fromTime = v;
                          })
                        }),
                      ],
                    ),
                    SizedBox(height: 20),
                    Text(Constants.TO, style: TextStyle(fontWeight: FontWeight.bold)),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        DatePicker(value: toDate, onChange: (DateTime v) => {
                          setState(() {
                            toDate = v;
                          })
                        }),
                        TimePicker(value: toTime, onChange: (TimeOfDay v) => {
                          setState(() {
                            toTime = v;
                          })
                        }),
                      ],
                    ),
                    SizedBox(height: 10),
                    Container(
                      height: 5 * 25.0,
                      child: TextField(
                        controller: messageController,
                        maxLines: 5,
                        maxLength: 120,
                        style: TextStyle(color: Colors.black, fontSize: 17),
                        decoration: InputDecoration(
                          floatingLabelBehavior: FloatingLabelBehavior.always,
                          labelText:Constants.MESSAGE,
                          hintText: Constants.ENTER_MESSAGE,
                          isDense: false,
                          focusColor: Colors.red
                        ),
                      )
                    )
                  ]
                ),
              ),
            ],
          ),
        ),
        loading == true ? LoadingIndicator() : Container()
      ],)
    );
  }
}
