// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

import 'package:Linccup/forgot/forgot_password_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:auto_route/auto_route.dart';
import 'package:Linccup/splash_screen.dart';
import 'package:Linccup/login/login_screen.dart';
import 'package:user_repository/user_repository.dart';
import 'package:Linccup/screens/settings_screen.dart';
import 'package:Linccup/screens/home/home_screen.dart';
import 'package:Linccup/screens/contacts_screen.dart';
import 'package:Linccup/screens/linccup_invite_screen.dart';
import 'package:Linccup/screens/welcome_screen.dart';

abstract class Routes {
  static const splashScreen = '/splash-screen';
  static const loginScreen = '/';
  static const forgotPasswordScreen = '/forgot-password-screen';
  static const settingsScreen = '/settings-screen';
  static const homeScreen = '/home-screen';
  static const contactsScreen = '/contacts-screen';
  static const linccupInviteScreen = '/linccup-invite-screen';
  static const welcomeScreen = '/welcome-screen';
  static const all = {
    splashScreen,
    loginScreen,
    forgotPasswordScreen,
    settingsScreen,
    homeScreen,
    contactsScreen,
    linccupInviteScreen,
    welcomeScreen,
  };
}

class Router extends RouterBase {
  @override
  Set<String> get allRoutes => Routes.all;

  @Deprecated('call ExtendedNavigator.ofRouter<Router>() directly')
  static ExtendedNavigatorState get navigator =>
      ExtendedNavigator.ofRouter<Router>();

  @override
  Route<dynamic> onGenerateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case Routes.splashScreen:
        return MaterialPageRoute<dynamic>(
          builder: (context) => SplashScreen(),
          settings: settings,
        );
      case Routes.loginScreen:
        if (hasInvalidArgs<LoginScreenArguments>(args, isRequired: true)) {
          return misTypedArgsRoute<LoginScreenArguments>(args);
        }
        final typedArgs = args as LoginScreenArguments;
        return MaterialPageRoute<dynamic>(
          builder: (context) => LoginScreen(
              key: typedArgs.key, userRepository: typedArgs.userRepository),
          settings: settings,
        );
      case Routes.forgotPasswordScreen:
        return MaterialPageRoute<dynamic>(
          builder: (context) => ForgotPasswordScreen(
            userRepository: null,
          ),
          settings: settings,
        );
      case Routes.settingsScreen:
        return MaterialPageRoute<dynamic>(
          builder: (context) => SettingsScreen(),
          settings: settings,
        );
      case Routes.homeScreen:
        if (hasInvalidArgs<HomeScreenArguments>(args, isRequired: true)) {
          return misTypedArgsRoute<HomeScreenArguments>(args);
        }
        return MaterialPageRoute<dynamic>(
          builder: (context) => HomeScreen(),
          settings: settings,
        );
      case Routes.contactsScreen:
        return MaterialPageRoute<dynamic>(
          builder: (context) => ContactsScreen(),
          settings: settings,
        );
      case Routes.linccupInviteScreen:
        if (hasInvalidArgs<LinccupInviteScreenArguments>(args,
            isRequired: true)) {
          return misTypedArgsRoute<LinccupInviteScreenArguments>(args);
        }
        final typedArgs = args as LinccupInviteScreenArguments;
        return MaterialPageRoute<dynamic>(
          builder: (context) => LinccupInviteScreen(
            key: typedArgs.key,
            meet: typedArgs.meet,
            refresh: null,
          ),
          settings: settings,
        );
      case Routes.welcomeScreen:
        return MaterialPageRoute<dynamic>(
          builder: (context) => WelcomeScreen(),
          settings: settings,
        );
      default:
        return unknownRoutePage(settings.name);
    }
  }
}

// *************************************************************************
// Arguments holder classes
// **************************************************************************

//LoginScreen arguments holder class
class LoginScreenArguments {
  final Key key;
  final UserRepository userRepository;
  LoginScreenArguments({this.key, @required this.userRepository});
}

//HomeScreen arguments holder class
class HomeScreenArguments {
  final Key key;
  final String name;
  HomeScreenArguments({this.key, @required this.name});
}

//LinccupInviteScreen arguments holder class
class LinccupInviteScreenArguments {
  final Key key;
  final String meet;
  LinccupInviteScreenArguments({this.key, @required this.meet});
}
