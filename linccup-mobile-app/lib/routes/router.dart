import 'package:Linccup/forgot/forgot_password_screen.dart';
import 'package:Linccup/screens/contacts_screen.dart';
import 'package:Linccup/screens/home/home_screen.dart';
import 'package:Linccup/login/login_screen.dart';
import 'package:Linccup/screens/welcome_screen.dart';
import 'package:Linccup/splash_screen.dart';
import 'package:auto_route/auto_route_annotations.dart';
import 'package:Linccup/screens/settings_screen.dart';
import 'package:Linccup/screens/linccup_invite_screen.dart';

@MaterialAutoRouter()
class $Router {
  SplashScreen splashScreen;

  @initial
  LoginScreen loginScreen;
  ForgotPasswordScreen forgotPasswordScreen;
  SettingsScreen settingsScreen;
  HomeScreen homeScreen;
  ContactsScreen contactsScreen;
  LinccupInviteScreen linccupInviteScreen;
  WelcomeScreen welcomeScreen;
}
