import 'package:flutter/material.dart';
import 'package:Linccup/common/constants.dart' as Constants;

class RegisterButton extends StatelessWidget {
  final VoidCallback _onPressed;

  RegisterButton({Key key, VoidCallback onPressed})
      : _onPressed = onPressed,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      // shape: RoundedRectangleBorder(
      //   borderRadius: BorderRadius.circular(30.0),
      // ),
      color: Theme.of(context).primaryColor,
      onPressed: _onPressed,
      child: Text(Constants.REGISTER, style: TextStyle(color: Colors.white)),
    );    
  }
}
