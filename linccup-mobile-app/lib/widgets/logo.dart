import 'package:flutter/material.dart';
import 'package:Linccup/common/constants.dart' as Constants;

class Logo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 20),
      height: 200,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(height: 10),
          Expanded(
            child: Image.asset(
              'assets/linccup-logo.png',
              // height: ,
              // color: Theme.of(context).primaryColor,
            )
          ),
          SizedBox(height: 10),
          Text(Constants.LINCCUP.toUpperCase(), style: TextStyle(fontWeight: FontWeight.bold, color: Color(0xFF2C2C2C), fontSize: 30)),
        ]
      )
    );
  }
}
