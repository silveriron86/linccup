import 'package:flutter/material.dart';
import 'package:linccups_repository/linccups_repository.dart';
import 'package:Linccup/common/constants.dart' as Constants;

class DeleteLinccupSnackBar extends SnackBar {
  DeleteLinccupSnackBar({
    Key key,
    @required Linccup linccup,
    @required VoidCallback onUndo,
  }) : super(
          key: key,
          content: Text(
            '${Constants.DELETED} ${linccup.summary}',
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
          ),
          duration: Duration(seconds: 2),
          action: SnackBarAction(
            label: Constants.UNDO,
            onPressed: onUndo,
          ),
        );
}
