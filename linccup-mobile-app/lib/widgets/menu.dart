import 'package:Linccup/blocs/authentication_bloc/bloc.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Menu extends StatelessWidget {
  final FirebaseUser firebaseUser;
  final Function onGetImage;

  const Menu({Key key, @required this.firebaseUser, @required this.onGetImage})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 250,
      child: Drawer(
        child: ListView(
          children: <Widget>[
            UserAccountsDrawerHeader(
              accountEmail: Text('${firebaseUser?.email}'),
              accountName: Text("Account Name: ${firebaseUser?.displayName}"),
              currentAccountPicture: GestureDetector(
                  onTap: onGetImage,
                  child: CircleAvatar(
                      radius: 100,
                      backgroundImage: NetworkImage(
                        firebaseUser.photoUrl != null
                            ? firebaseUser.photoUrl
                            : 'https://www.w3schools.com/w3images/avatar6.png',
                      ))),
            ),
            ListTile(
              leading: Icon(Icons.settings),
              title: Text('Settings'),
              onTap: () {
                Navigator.pushNamed(context, '/settings-screen');
              },
            ),
            ListTile(
              leading: Icon(Icons.exit_to_app),
              title: Container(
                alignment: Alignment.centerLeft,
                child: ButtonTheme(
                  minWidth: 0,
                  height: 0,
                  materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  padding: EdgeInsets.zero,
                  child: FlatButton(
                      child: Text('Logout'),
                      onPressed: () {
                        BlocProvider.of<AuthenticationBloc>(context)
                            .add(LoggedOut());
                      }),
                ),
              ),
            ),
            AboutListTile(
              icon: Icon(Icons.info),
            ),
          ],
        ),
      ),
    );
  }
}
