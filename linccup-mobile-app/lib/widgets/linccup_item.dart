import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:linccups_repository/linccups_repository.dart';
import 'package:Linccup/common/constants.dart' as Constants;

class LinccupItem extends StatelessWidget {
  final DismissDirectionCallback onDismissed;
  final GestureTapCallback onTap;
  final ValueChanged<bool> onCheckboxChanged;
  final Linccup linccup;

  LinccupItem({
    Key key,
    @required this.onDismissed,
    @required this.onTap,
    @required this.onCheckboxChanged,
    @required this.linccup,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dismissible(
        key: Key('__linccup_item_${linccup.id}'),
        onDismissed: onDismissed,
        child:
            Stack(alignment: AlignmentDirectional.topStart, children: <Widget>[
          ListTile(
              onTap: onTap,
              // leading: Checkbox(
              //   value: linccup.complete,
              //   onChanged: onCheckboxChanged,
              // ),
              leading: Container(
                  width: 60,
                  height: 60,
                  child: (Image.asset('assets/images/avatar.png',
                      fit: BoxFit.contain))),
              // title: Hero(
              //   tag: '${linccup.id}__heroTag',
              //   child: Container(
              //     width: MediaQuery.of(context).size.width,
              //     child: Text(
              //       linccup.summary,
              //       style: Theme.of(context).textTheme.title,
              //     ),
              //   ),
              // ),
              title: Text(linccup.type),
              subtitle: Text(
                      '${linccup.fromTime.isEmpty ? 'NONE' : linccup.fromTime} @ ${linccup.address}',
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: Theme.of(context).textTheme.subtitle1,
                    ),
              trailing: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(Constants.INVITED),
                    Text(Constants.DECLINED)
                  ])),
          Container(
            height: 1,
            margin: EdgeInsets.only(left: 90, top: 63, right: 10),
            decoration: BoxDecoration(
              color: Theme.of(context).dividerColor,
            ),
            child: null,
          )
        ]));
  }
}
