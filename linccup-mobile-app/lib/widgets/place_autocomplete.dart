import 'dart:convert';

import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:flutter/material.dart';
import 'package:Linccup/common/constants.dart' as Constants;
import 'package:Linccup/common/keys.dart' as Keys;
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart' as http;

class PlaceAutocomplete extends StatefulWidget {
  final Function(String) onChange;

  PlaceAutocomplete({Key key, @required this.onChange}) : super(key: key);

  @override
  _PlaceAutocompleteState createState() => _PlaceAutocompleteState();
}

class _PlaceAutocompleteState extends State<PlaceAutocomplete> {
  Position _currentPosition;
  AutoCompleteTextField<dynamic> acField;
  GlobalKey key = new GlobalKey<AutoCompleteTextFieldState<dynamic>>();

  List < PredictionsList > autocomFromJson(String str) =>
  List < PredictionsList > .from(
    json.decode(str)["predictions"].map((x) => PredictionsList.fromJson(x)));

  _PlaceAutocompleteState() {
    acField = new AutoCompleteTextField<dynamic>(
      decoration: new InputDecoration(
        border: InputBorder.none,
        hintText: Constants.LOCATION,
        hintStyle: TextStyle(fontSize: 15),
        suffixIcon: null
      ),
      style: TextStyle(fontSize: 15),
      itemSubmitted: (item) => _onChanged(item),
      key: key,
      suggestions: [],
      itemBuilder: (context, suggestion) => new Padding(
          child: new ListTile(
              title: new Text(suggestion.description)),
          padding: EdgeInsets.all(8.0)),
      itemSorter: (a, b) => a.description.compareTo(b.description),
      itemFilter: (suggestion, input) =>
          (suggestion.description).toLowerCase().startsWith(input.toLowerCase()),
      clearOnSubmit: false,
      textChanged: (input) => {
        _updateSuggestions(input)
      }
    );
  }

  @override
  void initState() {
    super.initState();
    Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.best).then((result) {
      setState(() {
        _currentPosition = result;
      });
      _getAddressFromLatLng();
    });
  }

  _getAddressFromLatLng() async {
    try {
      List<Placemark> p = await Geolocator().placemarkFromCoordinates(
          _currentPosition.latitude, _currentPosition.longitude);
      Placemark place = p[0];
      acField.textField.controller.text = "${place.locality}, ${place.postalCode}, ${place.country}";
      widget.onChange(acField.textField.controller.text);
    } catch (e) {
      print(e);
    }
  }

  _updateSuggestions(String input) async {
    var url = 'https://maps.googleapis.com/maps/api/place/autocomplete/json?input=$input&key=${Keys.kGoogleApiKey}';
    final response = await http.get(url);

    if (response.statusCode == 200) {
      var responseJson = autocomFromJson(response.body);
      acField.updateSuggestions(responseJson);
    } else {
      throw Exception('Failed to load album');
    }
  }

  _onChanged(value) {
    acField.textField.controller.text = value.description;
    widget.onChange(acField.textField.controller.text);
  }

  @override
  Widget build(BuildContext context) {
    return acField;
  }
}

class PredictionsList {
  String id;
  String description;
  String placeId;
  String reference;

  PredictionsList({
    this.id,
    this.placeId,
    this.reference,
    this.description,
  });

  factory PredictionsList.fromJson(Map < String, dynamic > json) => PredictionsList(
    id: json["id"],
    placeId: json["place_id"],
    reference: json["reference"],
    description: json["description"],
  );

  Map < String, dynamic > toJson() => {
    "id": id,
    "placeId": placeId,
    "reference": reference,
    "description": description,
  };
}