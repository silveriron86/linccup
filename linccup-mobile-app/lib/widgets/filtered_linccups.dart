import 'package:Linccup/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:Linccup/blocs/blocs.dart';

class FilteredLinccups extends StatelessWidget {
  FilteredLinccups({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FilteredLinccupsBloc, FilteredLinccupsState>(
      builder: (context, state) {
        if (state is FilteredLinccupsLoading) {
          return LoadingIndicator();
        } else if (state is FilteredLinccupsLoaded) {
          final linccups = state.filteredLinccups;
          return Container(
            height: 600,
            child: ListView.builder(
              itemCount: linccups.length,
              itemBuilder: (context, index) {
                final linccup = linccups[index];
                return LinccupItem(
                  linccup: linccup,
                  onDismissed: (direction) {
                    BlocProvider.of<LinccupsBloc>(context)
                        .add(DeleteLinccup(linccup));
                    Scaffold.of(context).showSnackBar(DeleteLinccupSnackBar(
                      linccup: linccup,
                      onUndo: () => BlocProvider.of<LinccupsBloc>(context)
                          .add(AddLinccup(linccup)),
                    ));
                  },
                  onTap: () async {
                    // final removedLinccup = await Navigator.of(context).push(
                    //   MaterialPageRoute(builder: (_) {
                    //     return DetailsScreen(id: linccup.id);
                    //   }),
                    // );
                    // if (removedLinccup != null) {
                    //   Scaffold.of(context).showSnackBar(
                    //     DeleteLinccupSnackBar(
                    //       linccup: linccup,
                    //       onUndo: () => BlocProvider.of<LinccupsBloc>(context)
                    //           .add(AddLinccup(linccup)),
                    //     ),
                    //   );
                    //}
                  },
                  onCheckboxChanged: (_) {
                    BlocProvider.of<LinccupsBloc>(context).add(
                      UpdateLinccup(
                          linccup.copyWith(complete: !linccup.complete)),
                    );
                  },
                );
              },
            ),
          );
        } else {
          return Container();
        }
      },
    );
  }
}
