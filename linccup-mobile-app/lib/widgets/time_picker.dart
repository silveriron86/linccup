import 'package:flutter/material.dart';

class TimePicker extends StatefulWidget {
  final TimeOfDay value;
  final Function(TimeOfDay) onChange;

  TimePicker({Key key, @required this.value, @required this.onChange}) : super(key: key);

  @override
  _TimePickerState createState() => _TimePickerState();
}

class _TimePickerState extends State<TimePicker> {
  TimeOfDay _selected;

  Future<Null> onSelectTime(BuildContext context) async {
    final TimeOfDay picked = await showTimePicker(
        context: context,
        initialTime: _selected,);
    if (picked != null && picked != _selected)
      setState(() {
        _selected = picked;
      });
      widget.onChange(picked);
  }

  DateTime getDateTime(TimeOfDay tod) {
    final now = new DateTime.now();
    final dt = DateTime(now.year, now.month, now.day, tod.hour, tod.minute);
    return dt;
  }  

  @override
  void initState() {
    _selected = widget.value;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(border: Border(bottom: BorderSide(color: Theme.of(context).dividerColor))),
      padding: EdgeInsets.only(top: 10, bottom: 2),
      margin: EdgeInsets.only(bottom: 8),
      child: InkWell(
        onTap: () => onSelectTime(context),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Text(
              "${TimeOfDay.fromDateTime(getDateTime(_selected)).format(context)}",
              style: TextStyle(fontSize: 16, color: Color(0XFF939393))
            ),
            SizedBox(width: 10),
            Icon(Icons.arrow_drop_down, size: 30, color: Color(0xFF939393))
          ]
        )
      ),
    );
  }
}