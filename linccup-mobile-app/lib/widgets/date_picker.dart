import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class DatePicker extends StatefulWidget {
  final DateTime value;
  final Function(DateTime) onChange;

  DatePicker({Key key, @required this.value, @required this.onChange}) : super(key: key);

  @override
  _DatePickerState createState() => _DatePickerState();
}

class _DatePickerState extends State<DatePicker> {
  DateTime _selected;

  Future<Null> onSelectDate(BuildContext context) async {
    int curYear = DateTime.now().year;
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: _selected,
        firstDate: DateTime(curYear),
        lastDate: DateTime(curYear + 30));
    if (picked != null && picked != _selected)
      setState(() {
        _selected = picked;
      });
      widget.onChange(picked);
  }

  @override
  void initState() {
    _selected = widget.value;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(border: Border(bottom: BorderSide(color: Theme.of(context).dividerColor))),
      padding: EdgeInsets.only(top: 10, bottom: 2),
      margin: EdgeInsets.only(bottom: 8),
      child: InkWell(
        onTap: () => onSelectDate(context),
        child: Row(
          children: <Widget>[
            Text(
              "${DateFormat.yMMMMEEEEd().format(_selected)}",
              style: TextStyle(fontSize: 16, color: Color(0XFF939393))
            ),
            SizedBox(width: 10),
            Icon(Icons.arrow_drop_down, size: 30, color: Color(0xFF939393))
          ]
        )
      ),
    );
  }
}