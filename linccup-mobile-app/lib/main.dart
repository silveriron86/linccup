import 'package:Linccup/blocs/authentication_bloc/bloc.dart';
import 'package:Linccup/blocs/tab/tab_bloc.dart';
import 'package:Linccup/screens/home/home_screen.dart';
import 'package:Linccup/login/login.dart';
import 'package:Linccup/screens/settings_screen.dart';
import 'package:Linccup/screens/welcome_screen.dart';

import 'package:Linccup/splash_screen.dart';

import 'package:flutter/material.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive/hive.dart';
import 'package:linccups_repository/linccups_repository.dart';

import 'package:user_repository/user_repository.dart';

import 'blocs/blocs.dart';
import 'hive/settings.model.dart';
import 'common/globals.dart' as globals;

void main() {
  globals.isRegistered = false;
  WidgetsFlutterBinding.ensureInitialized();
  BlocSupervisor.delegate = SimpleBlocDelegate();
  final UserRepository userRepository = FirebaseUserRepository();
  Hive.registerAdapter(SettingsModelAdapter());
  runApp(MultiBlocProvider(
    providers: [
      BlocProvider(
        create: (context) => AuthenticationBloc(
          userRepository: userRepository,
        )..add(AppStarted()),
      ),
      BlocProvider<LinccupsBloc>(
        create: (context) => LinccupsBloc(
          linccupsRepository: FirebaseLinccupRepository(),
        )..add(LoadLinccups()),
      ),
    ],
    child: App(userRepository: userRepository),
  ));
}

class App extends StatelessWidget {
  final UserRepository _userRepository;

  App({Key key, @required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    Map<int, Color> mainColor = {
      50: Color.fromRGBO(131, 0, 0, .1),
      100: Color.fromRGBO(131, 0, 0, .2),
      200: Color.fromRGBO(131, 0, 0, .3),
      300: Color.fromRGBO(131, 0, 0, .4),
      400: Color.fromRGBO(131, 0, 0, .5),
      500: Color.fromRGBO(131, 0, 0, .6),
      600: Color.fromRGBO(131, 0, 0, .7),
      700: Color.fromRGBO(131, 0, 0, .8),
      800: Color.fromRGBO(131, 0, 0, .9),
      900: Color.fromRGBO(131, 0, 0, 1),
    };
    const primaryColor = 0xFF830000;
    const greyColor = 0xFFE9E9E9;
    const darkColor = 0xFF504D4D;
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Color(primaryColor),
        primaryColorLight: Color(0xFFAB5252),
        primaryColorDark: Color(darkColor),
        secondaryHeaderColor: Color(0xFF8F8F8F),
        scaffoldBackgroundColor: Colors.white,
        dividerColor: Color(greyColor),
        primarySwatch: MaterialColor(primaryColor, mainColor),
        appBarTheme: AppBarTheme(
          color: Color(primaryColor),
        ),
        textTheme: TextTheme(bodyText2: TextStyle(color: Color(0xFF898686))),
        inputDecorationTheme: InputDecorationTheme(
            // border: InputBorder.none,//(borderSide: BorderSide(color: Colors.red)),
            enabledBorder: new UnderlineInputBorder(
                borderSide: new BorderSide(color: Color(greyColor))),
            labelStyle: TextStyle(color: Color(darkColor), fontSize: 26.0),
            hintStyle: TextStyle(color: Color(0xFF9E9E9E))),
      ),
      home: BlocBuilder<AuthenticationBloc, AuthenticationState>(
        builder: (context, state) {
          if (state is Unauthenticated) {
            return LoginScreen(userRepository: _userRepository);
          }
          if (state is Authenticated) {
            return MultiBlocProvider(
              providers: [
                BlocProvider<TabBloc>(
                  create: (context) => TabBloc(),
                ),
                BlocProvider<FilteredLinccupsBloc>(
                  create: (context) => FilteredLinccupsBloc(
                    linccupsBloc: BlocProvider.of<LinccupsBloc>(context),
                  ),
                ),
                // BlocProvider<StatsBloc>(
                //   create: (context) => StatsBloc(
                //     linccupsBloc: BlocProvider.of<LinccupsBloc>(context),
                //   ),
                //),
              ],
              child:
                  globals.isRegistered == true ? WelcomeScreen() : HomeScreen(),
            );
          }
          return SplashScreen();
        },
      ),
      routes: {
        //route setting
        '/settings-screen': (context) => SettingsScreen(),
        '/home-screen': (context) => HomeScreen()
      },
    );
  }
}
