import 'package:Linccup/register/register.dart';

import 'package:user_repository/user_repository.dart';
import 'package:flutter/material.dart';
import 'package:Linccup/common/constants.dart' as Constants;

class CreateAccountButton extends StatelessWidget {
  final FirebaseUserRepository _userRepository;

  CreateAccountButton({Key key, @required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      child: Text(
        Constants.SIGN_UP,
      ),
      onPressed: () {
        Navigator.of(context).push(
          MaterialPageRoute(builder: (context) {
            return RegisterScreen(userRepository: _userRepository);
          }),
        );
      },
    );
  }
}
