import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:Linccup/login/login.dart';
import 'package:Linccup/common/constants.dart' as Constants;

class GoogleLoginButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      color: Theme.of(context).primaryColor,
      onPressed: () {
        BlocProvider.of<LoginBloc>(context).add(
          LoginWithGooglePressed(),
        );
      },
      child: Text(Constants.SIGN_GOOGLE, style: TextStyle(color: Colors.white)),
    );
  }
}
