import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class Notification extends Equatable {
  final int id;
  final String title;
  final String body;
  final String payload;

  Notification({
    @required this.id,
    @required this.title,
    @required this.body,
    @required this.payload,
  });

  @override
  List<Object> get props => [
        id,
        title,
        body,
        payload,
      ];
}
