import 'package:hive/hive.dart';

part 'settings.model.g.dart';

@HiveType(typeId: 0)
class SettingsModel {
  @HiveField(0)
  bool notifications;

  @HiveField(1)
  bool incognitoMode;

  SettingsModel(this.notifications, this.incognitoMode);
}