import 'package:rxdart/rxdart.dart';
import 'package:meta/meta.dart';
import './apis.dart';

class FirebaseBloc {
  
  FirebaseBloc({@required this.apis});

  final _securityPubSub = PublishSubject<bool>();
  final Apis apis;

  Observable<bool> get loginStatus => _securityPubSub.stream;

  void login(String email, String password) async {
    try{
      print('$email and $password being submitted in firebase_bloc.login before login');
      await apis.login(email, password);
      print(apis.securityToken());
      _securityPubSub.sink.add(true);
    } catch (err) {
      _securityPubSub.sink.addError(err.toString());
    }
  }

  
  void dispose(){
    _securityPubSub.close();
  } 
}