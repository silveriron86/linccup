import 'dart:convert';

import 'package:linccup/models/models.dart';
import 'package:http/http.dart' as http;


class Apis{

  static const String server = 'https://www.googleapis.com';
  static const String key = 'AIzaSyAt8VtUUrJ0KxTpogEEu2Qu7yiVfWYzSOU';

  String _securityToken;

  Future login(String email, String password) async {
   final String url =
        '$server/identitytoolkit/v3/relyingparty/verifyPassword?key=$key';
    print('url: $url');
    var response = await http.post(
      url, 
      headers: {"Content-Type": "application/json"},
      body: json.encode({
        'email': email,
        'password': password,
        'returnSecureToken': true,
      }),
    );

  print(response.body); 


    

    
    _checkStatus(response);

    

    var map = json.decode(response.body);
   
    _securityToken = LoginResponse.fromMap(map).idToken;
    print(LoginResponse.fromMap(map).refreshToken);
    print('securityToken: $_securityToken');
  }

  

  void _checkStatus(http.Response response){
      print('statusCode is 200, its ok');
      if (response.statusCode != 200){
        throw Exception('Error: ${response.statusCode} ${response.body}');
      }

   
  }

  Map<String, String> _createHeader() {
    if (_securityToken != null){
      var header = {
        "authorization": "Bearer $_securityToken",
        "Content-Type": "application/json",
      };
      return header;
    }
    return {"Content-Type": "application/json"};
  }

  String securityToken(){
    return _securityToken;
  }

}