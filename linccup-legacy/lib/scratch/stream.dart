import 'dart:async';

void main(){

  final StreamController ctrl = StreamController();
  final StreamSubscription subscription = ctrl.stream.listen((data)=> print('$data'));

  ctrl.sink.add('my name is Mac');
  ctrl.sink.add(1234);
  ctrl.sink.add({'a':'elementA', 'b':'element B'});
  ctrl.sink.add(123.45);

  ctrl.close();

}