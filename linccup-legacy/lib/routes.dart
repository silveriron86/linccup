import 'package:flutter/material.dart';
import 'package:linccup/pages/types/types_page.dart';
import './pages/index.dart';


final routes ={
  '/': (_) => LoginPage(),
  '/digest': (_) => DigestPage(),
  '/icons': (_) => IconsPage(),
  '/linccup': (_) => LinccupPage(),
  '/linccups':(_) => LinccupsPage(),
  '/messages': (_) => MessagesListPage(),
  '/messages_list': (_) => MessagesListPage(),
  '/settings': (_) => SettingsPage(),
  '/types': (_) => TypesPage(),
};

final RouteObserver<PageRoute> routeObserver = RouteObserver<PageRoute>();

