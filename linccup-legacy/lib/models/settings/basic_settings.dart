import 'package:meta/meta.dart';
//import 'package:linccup/uuid.dart';

@immutable
class BasicSettings {
  final int userId;
  final String displayName;
  final DateTime timeZone;
  
  Map<String, dynamic> toMap() => {
    'userId': userId,
    'displayName': displayName,
    'timeZone': timeZone,
  };

  factory BasicSettings.fromMap(Map<String, dynamic> map) => BasicSettings(
    userId: map['userId'],
    displayName: map['displayName'],
    timeZone: map['timeZone'],
  );

  BasicSettings({@required this.userId, displayName, timeZone})
        : this.displayName = displayName ?? '',
          this.timeZone = timeZone ?? '';

  BasicSettings.fromData(Map<String, dynamic> data) 
      : userId = data['userId'],
        displayName = data['displayName'],
        timeZone = data['timeZone'];

} 