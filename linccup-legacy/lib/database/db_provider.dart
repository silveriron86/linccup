import 'dart:io';
import 'dart:async';
import 'package:linccup/models/linccup.dart';
import 'package:linccup/models/settings/basic_settings.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

class DbProvider {
  Database _database;

  Future<Database> get database async {
    if (_database == null){
      _database = await _initialize();
    }
    return _database;
  }



  Future<int> createLinccup(Linccup linccup) async {
    final db = await database;
    return await db.insert('Linccup', linccup.toMap());
  }

  Future<int> updateLinccup(Linccup linccup) async {
    final db = await database;
    return await db.update('Linccup', linccup.toMap(),
      where: 'id = ?', whereArgs: [linccup.id]
    );
  }

  Future<List<Linccup>> getAllLinccups() async{
    final db = await database;
    var res = await db.query('Linccup');
    List<Linccup> list = res.isNotEmpty?
      res.map((l)=> Linccup.fromMap(l)).toList():[];
    return list;
  }

  Future<int> createBasicSettings(BasicSettings basicSettings) async {
    final db = await database;
    return await db.insert('BasicSettings', basicSettings.toMap());
  }

  Future<int> updateBasicSettings(BasicSettings basicSettings) async {
    final db = await database;
    return await db.update('BasicSettings', basicSettings.toMap(),
      where: 'userId = ?', whereArgs: [basicSettings.userId]
    );
  }

  
  void dispose(){
    _database?.close();
    _database = null;
  }

  Future<Database> _initialize() async{
    Directory docDir = await getApplicationDocumentsDirectory();
    String path = join(docDir.path, 'linccup.db');
    Database db = await openDatabase(
      path,
      version: 1,
      onOpen: (db){
        print('Database Open');
      },
      onCreate: _onCreate,
    );

    return db;

  }

  void _onCreate(Database db, int verision) async{
    await db.execute("CREATE TABLE Linccup("
    "id INTEGER PRIMARY KEY,"
    "complete INTEGER,"
    "userIDs BLOB,"
    "creatorUserID TEXT,"
    "dateTime TEXT,"
    "location TEXT,"
    ")");
    await db.execute("CREATE TABLE BasicSettings("
    "id INTEGER PRIMARY KEY,"
    "displayName TEXT,"
    "timeZone TEXT,"
    ")");
  }
}