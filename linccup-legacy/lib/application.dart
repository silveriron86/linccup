import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:linccup/firebase/apis.dart';
import 'package:linccup/firebase/firebase_bloc.dart';
import './routes.dart';
import 'database/db_provider.dart';

class Application extends StatelessWidget {
  @override 
  Widget build(BuildContext context) {
    return Provider<DbProvider>(
        builder: (_) => DbProvider(),
        dispose: (_, value) => value.dispose(),
          child: Provider<FirebaseBloc>(
        builder: (_)=> FirebaseBloc(apis: Apis()),
        dispose: (_, value) => value.dispose(),
        child: MaterialApp(
          title: 'Linccup',
          theme: ThemeData(
            primarySwatch: Colors.lightGreen,
          ),
          initialRoute: '/linccup',
          routes: routes,
          navigatorObservers: [routeObserver],
        ),
      ),
    );
  }
}