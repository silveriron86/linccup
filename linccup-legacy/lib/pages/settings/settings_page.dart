import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:linccup/database/db_provider.dart';
import 'package:linccup/models/settings/basic_settings.dart';
import 'package:linccup/pages/icons/icon_holder.dart';
import 'package:linccup/pages/index.dart';
import 'package:provider/provider.dart';

class SettingsPage extends StatefulWidget {
  const SettingsPage ({Key key, this.basicSettings}) : super(key: key);
  final BasicSettings basicSettings;

  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  Map<String, dynamic> _data;
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  IconData _newIcon;

  @override 
  void initState(){
    super.initState();
    if(widget.basicSettings != null){
      _data = widget.basicSettings.toMap();
    } else {
      _data = Map<String, dynamic>();
    }
  }


  @override
  Widget build(BuildContext context) {
    var dbProvider = Provider.of<DbProvider>(context);
    return Scaffold(
      appBar: AppBar(
        title: const Text("Settings"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.save),
            onPressed: (){
              if(!_formKey.currentState.validate())return;
              _formKey.currentState.save();
        
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
      body: Form(
          key: _formKey,
          child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            children: <Widget>[
              IconHolder(
                newIcon: _newIcon,
                onIconChange: (IconData iconData) {
                  setState((){
                    _newIcon = iconData;
                  }); 
                } ,
              ),
              TextFormField(
                initialValue: widget.basicSettings != null ? widget.basicSettings.displayName:'',
                keyboardType: TextInputType.text,
                decoration:  InputDecoration(
                  labelText: "DisplayName",
                  border: OutlineInputBorder(),
                ),
                validator: (String value){ 
                  if(value.isEmpty) {
                    return 'Required';
                  }
                },
                onSaved: (String value) => _data['displayName'] = value,
              ),
              TextFormField(
                 initialValue: widget.basicSettings != null ? widget.basicSettings.timeZone:'',
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                  labelText: "TimeZone",
                  border: OutlineInputBorder(),
                ),
                validator: (String value){
                  if(value.isEmpty){ 
                    return 'Required';
                  }
                },
                onSaved: (String value) => _data['timeZone'] = value,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

