import 'package:flutter/material.dart';
import 'package:flutter/semantics.dart';
import 'package:linccup/pages/settings/settings_page.dart';
import 'package:linccup/widgets/menu.dart';
import 'dart:math' as math;

class LinccupsPage extends StatelessWidget {
  LinccupsPage({Key key}) : super(key: key);
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  
  
  void _openDrawer(){
     _scaffoldKey.currentState.openDrawer();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Menu(),
      drawerScrimColor: Colors.white38,
      body: CollapsingList(openDrawer: _openDrawer,), 
      
          
          // Container(
          //   height: 300,
          //   child: MockLinccups(),
          // )
       
    );
  
  }
}

class MockLinccups extends StatelessWidget {
  const MockLinccups({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
  padding: const EdgeInsets.all(8),
  children: <Widget>[
    Container(
      height: 50,
      color: Colors.amber[600],
      child: const Center(child: Text('Entry A')),
    ),
    Container(
      height: 50,
      color: Colors.amber[500],
      child: const Center(child: Text('Entry B')),
    ),
    Container(
      height: 50,
      color: Colors.amber[100],
      child: const Center(child: Text('Entry C')),
    ),
    Container(
      height: 50,
      color: Colors.amber[600],
      child: const Center(child: Text('Entry A')),
    ),
    Container(
      height: 50,
      color: Colors.amber[500],
      child: const Center(child: Text('Entry B')),
    ),
    Container(
      height: 50,
      color: Colors.amber[100],
      child: const Center(child: Text('Entry C')),
    ),
    Container(
      height: 50,
      color: Colors.amber[600],
      child: const Center(child: Text('Entry A')),
    ),
    Container(
      height: 50,
      color: Colors.amber[500],
      child: const Center(child: Text('Entry B')),
    ),
    Container(
      height: 50,
      color: Colors.amber[100],
      child: const Center(child: Text('Entry C')),
    ),
    Container(
      height: 50,
      color: Colors.amber[600],
      child: const Center(child: Text('Entry A')),
    ),
    Container(
      height: 50,
      color: Colors.amber[500],
      child: const Center(child: Text('Entry B')),
    ),
    Container(
      height: 50,
      color: Colors.amber[100],
      child: const Center(child: Text('Entry C')),
    ),
  ],
);
  }
}

class _SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  _SliverAppBarDelegate({
    @required this.minHeight,
    @required this.maxHeight,
    @required this.child,
  });
  final double minHeight;
  final double maxHeight;
  final Widget child;
  @override
  double get minExtent => minHeight;
  @override
  double get maxExtent => math.max(maxHeight, minHeight);
  @override
  Widget build(
      BuildContext context, 
      double shrinkOffset, 
      bool overlapsContent) 
  {
    return new SizedBox.expand(child: child);
  }
  @override
  bool shouldRebuild(_SliverAppBarDelegate oldDelegate) {
    return maxHeight != oldDelegate.maxHeight ||
        minHeight != oldDelegate.minHeight ||
        child != oldDelegate.child;
  }
}

class CollapsingList extends StatelessWidget {
  CollapsingList({@required this.openDrawer});
  
  final Function openDrawer;

  SliverPersistentHeader makeHeader(String headerText) {
    return SliverPersistentHeader(
      pinned: true,
      delegate: _SliverAppBarDelegate(
        minHeight: 60.0,
        maxHeight: 100.0,
        child: Container(
            color: Colors.lightBlue, child: Center(child:
                Text(headerText))),
      ),
    );
  }
  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: <Widget>[
        // makeHeader('Header Section 1'),
        // SliverGrid.count(
        //   crossAxisCount: 2,
        //   children: [
        //     Container(color: Colors.redAccent, height: 200.0),
        //     Container(color: Colors.purple, height: 20.0),
        //     Container(color: Colors.green, height: 20.0),
        //     Container(color: Colors.orange, height: 20.0),
        //     Container(color: Colors.yellow, height: 20.0),
        //     Container(color: Colors.pink, height: 20.0),
        //     Container(color: Colors.cyan, height: 20.0),
        //     Container(color: Colors.indigo, height: 20.0),
        //     Container(color: Colors.blue, height: 20.0),
        //   ],
        // ),
        SliverAppBar(
                backgroundColor: Colors.redAccent,
                brightness: Brightness.dark,
                iconTheme: IconThemeData(color: Colors.white),
                title: Text("Linccups",style: TextStyle(color: Colors.white),),
                bottom: PreferredSize(child: Text("Bottom of the SliverAppBar", style: TextStyle(color: Colors.white),), preferredSize: Size.fromHeight(50.0)),
                expandedHeight: 110.0,
                flexibleSpace: FlexibleSpaceBar(
                  background: Image(
                    image: NetworkImage('https://pagethink.com/media/uploads/project-gallery-images/lg_page_uh_stadium_8nov2014_39.jpg'),
                    fit: BoxFit.cover,
                  ),
                ),
                floating: true,
                snap: true,
                primary: true,
                actions: <Widget>[
                  IconButton(
                    icon: Icon(Icons.settings_applications, color: Colors.white,), 
                    onPressed: () {
                      Navigator.push(context, MaterialPageRoute(builder: (context)=> SettingsPage()));
                    },
                  ),
                ],
                
                
        ),
       
        // Yes, this could also be a SliverFixedExtentList. Writing 
        // this way just for an example of SliverList construction.
        SliverList(
          delegate: SliverChildListDelegate(
            [
              Container(
                color: Colors.red[300], 
                height: 30.0,
                child: Row(
                  children: <Widget>[
                    Text(
                      "Oct 29th - Hallows Eve Day",
                      style: TextStyle(color: Colors.white),
                    ),
                  ],
                ),
              ),
       
              InkWell(
                    onTap: ()=> Navigator.of(context).pushNamed('/linccup'),
                    child: Container(
                    color: Colors.cyan, 
                    height: 40.0,
                    child: Row(
                      children: <Widget>[
                        Container(
                          child: Text(
                            "9:30am - 10:30am",
                            style: TextStyle(fontSize: 10.0),
                          ),
                          width: 50.0,
                        ),
                        Text("Breakfast - code and coffee "),
                      ],
                    ),
                  ),
              ),
             
              AddLinccupDate(),
              Container(
                color: Colors.red[300], 
                height: 30.0,
                child: Row(
                  children: <Widget>[
                    Text(
                      "Oct 31st",
                      style: TextStyle(color: Colors.white),
                    ),
                  ],
                ),
              ),
              Container(
                color: Colors.cyan, 
                height: 40.0,
                child: Row(
                  children: <Widget>[
                    Container(
                      child: Text(
                        "10:30am - 12:15pm",
                        style: TextStyle(fontSize: 10.0),
                      ),
                      width: 50.0,
                    ),
                    Text("Lunch with KR"),
                  ],
                ),
              ),
              AddLinccupDate(),
            ],
          ),
        ),
        makeHeader('Header Section 2'),
        SliverFixedExtentList(
          itemExtent: 30.0,
          delegate: SliverChildListDelegate(
            [
              Container(color: Colors.red),
              Container(color: Colors.purple),
              Container(color: Colors.green),
              Container(color: Colors.orange),
              Container(color: Colors.yellow),
            ],
          ),
        ),
        makeHeader('Header Section 3'),
        SliverGrid(
          gridDelegate: 
              new SliverGridDelegateWithMaxCrossAxisExtent(
            maxCrossAxisExtent: 200.0,
            mainAxisSpacing: 10.0,
            crossAxisSpacing: 10.0,
            childAspectRatio: 4.0,
          ),
          delegate: new SliverChildBuilderDelegate(
            (BuildContext context, int index) {
              return new Container(
                alignment: Alignment.center,
                color: Colors.teal[100 * (index % 9)],
                child: new Text('grid item $index'),
              );
            },
            childCount: 20,
          ),
        ),
        
      ],
    );
  }
}

class AddLinccupDate extends StatelessWidget {
  const AddLinccupDate({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Container(
        color: Colors.transparent, 
        height: 40.0,
        child: Center(
          child: Icon(Icons.add),
        ),
      ),
    );
  }
}


class LinccupCustomScrollView extends StatelessWidget{
  math.Random _random = math.Random();
  Color _color = Color(0xFFFFFFFF);

  Color getRandomColor(){
    math.Random _random = math.Random();
    Color _color = Color(0xFFFFFFFF);
    
    return _color = Color.fromARGB(
      _random.nextInt(256), 
      _random.nextInt(256), 
      _random.nextInt(256),
      _random.nextInt(256),
    );
  }
  
  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
            slivers: <Widget>[
              SliverAppBar(
                backgroundColor: Colors.orangeAccent,
                brightness: Brightness.dark,
                title: Text("Linccups"),
                leading: Icon(Icons.menu),
                bottom: PreferredSize(child: Icon(Icons.linear_scale,size: 60.0,), preferredSize: Size.fromHeight(50.0)),
                expandedHeight: 110.0,
                flexibleSpace: FlexibleSpaceBar(
                  background: Image(
                    image: NetworkImage('https://flutter.github.io/assets-for-api-docs/assets/widgets/owl.jpg'),
                    fit: BoxFit.cover,
                  ),
                ),
                floating: true,
                snap: true,
                primary: true,
              ),
              SliverAppBar(
                backgroundColor: Colors.orangeAccent,
                brightness: Brightness.light,
                title: Text("Linccups"),
                leading: Icon(Icons.menu),
                bottom: PreferredSize(child: Icon(Icons.linear_scale,size: 60.0,), preferredSize: Size.fromHeight(50.0)),
                expandedHeight: 90.0,
                floating: true,
                snap: true,
              ),
              SliverList(
                delegate: SliverChildBuilderDelegate( (BuildContext context, int index){
                    return Container(color: getRandomColor(), height: 150.0);
                },

                ),
              ),
            ],
          );
  }
}