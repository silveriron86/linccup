import 'package:flutter/material.dart';
import 'package:linccup/pages/types/type_page.dart';

class TypesPage extends StatelessWidget {
  const TypesPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Types'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.add),
            onPressed: (){
              Navigator.push(
                context, 
                MaterialPageRoute(
                  builder: (_) => TypePage(),
                ),
              );
            },
          ),
        ],
      ),
      body: Container(
        child: const Text('Types'),
      ),
    );
  }
}