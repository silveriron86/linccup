import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:meta/meta.dart';

class MessagePage extends StatefulWidget {
  final bool isPrivate;
  MessagePage({Key key,@required this.isPrivate}) : super(key: key);

  @override
  _MessagePageState createState() => _MessagePageState();
}

class _MessagePageState extends State<MessagePage> {
  Map<String, dynamic> _formData = Map<String, dynamic>();
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
 
  
  DateTime _dateTime = DateTime.now();

  @override 
  void initState(){
    super.initState();
    _formData['isPrivate'] = widget.isPrivate;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("MessagePage"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.save),
            onPressed: (){
              if(!_formKey.currentState.validate())return;
              _formKey.currentState.save();
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
      body: Form(
          key: _formKey,
          child: Padding(
          padding: const EdgeInsets.all(10.0),
            child: Column(
              children: <Widget>[
                TextFormField(
                  decoration: InputDecoration(labelText: 'To:'),
                  validator: (String value) => value.isEmpty ? 'Required' : null,
                  onSaved: (String value) => _formData['message_to'] = value,
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'Message'),
                  validator: (String value) => value.isEmpty ? 'Required' : null,
                  onSaved: (String value) => _formData['message_text'] = value,
                ),
                Row(
                  children: <Widget>[
                    Checkbox(
                      activeColor: Colors.pink,
                      value: _formData['isPrivate'],
                      onChanged: (bool value) {
                        setState((){
                          _formData['isPrivate'] = value;
                        });
                      },
                    ),
                    Text("Private Message"),
                  ],
                ),
                Row(
                  children: <Widget>[
                    IconButton(
                      icon: Icon(Icons.date_range),
                      onPressed: () async {
                        var date = await showDatePicker(
                          context: context,
                          initialDate: _dateTime,
                          firstDate: DateTime.now().add(Duration(days:-365)),
                          lastDate: DateTime.now().add(Duration(days:365)),
                        );

                        if(date == null) return;

                        setState((){
                          _dateTime = date;
                        });
                      },
                    ),
                    Text(DateFormat('MM/dd/yyyy').format(_dateTime)),
                  ],
                ),
                DropdownButtonFormField<int>(
                  value: _formData['reasonId'],
                  decoration: InputDecoration(
                    labelText: "Reason",
                  ),
                  items: [
                      DropdownMenuItem<int>(
                        value: 1,
                        child: const Text('Meet for coffee'),
                      ),
                      DropdownMenuItem<int>(
                        value: 2,
                        child: const Text('Business proposal'),
                      ),
                  ],
                  validator: (int value) => value == null ? 'Required': null,
                  onChanged: (int value) {
                    setState(() {
                      _formData['reasonId'] = value;
                    });
                    
                  },
                ),
                DropdownButtonFormField<int>(
                  value: _formData['typeId'],
                  decoration: InputDecoration(
                    labelText: 'Type',
                  ),
                  onChanged: (int value){
                    setState((){
                      _formData['typeId'] = value;
                    });
                  },
                  items: [
                      DropdownMenuItem<int>(
                        value: 1,
                        child: const Text('Colleague'),
                      ),
                      DropdownMenuItem<int>(
                        value: 2,
                        child: const Text('Friend'),
                      ),
                  ],
                  validator: (int value) => value == null ? 'Required': null,
                ),
              ],
          ),
        ),
      ),
    );
  }
}