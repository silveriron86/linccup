import 'package:flutter/material.dart';
import 'package:linccup/pages/settings/settings_page.dart';
import 'package:linccup/widgets/menu.dart';

import 'message_page.dart';


// Get all contacts on device
class MessagesListPage extends StatefulWidget {
  MessagesListPage({Key key}) : super(key: key);

  _MessagesListPageState createState() => _MessagesListPageState();
}

class _MessagesListPageState extends State<MessagesListPage> {
  var message = 'Schedule Linccup';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
          appBar: AppBar(
            title: Text("Messages List"),
            actions: <Widget>[IconButton(icon: Icon(Icons.settings_applications), onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context)=> SettingsPage()));
            },)],
          ),
          drawer: Menu(),
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
               _MessageList(message: message),
               Container(
                 padding: EdgeInsets.only(bottom: 70),
                 height: MediaQuery.of(context).size.height - 220,
                 child: Row(
                   crossAxisAlignment: CrossAxisAlignment.start,
                   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                   children: <Widget>[
                   
                     _MessageBar(
                      100, Colors.cyanAccent, "Welcome!",
                     ),
     
                     _MessageBar(
                       400, Colors.deepPurpleAccent, "Purple!"
                     ),
           
                   ],
                  )
               ),
            ],
          ),
          floatingActionButton: PopupMenuButton(
            child: Icon(
              Icons.add_circle,
              size: 60,
              color: Theme.of(context).primaryColor,
            ),
            itemBuilder: (_)=> [ 
              PopupMenuItem(
                value: 1,
                child: const Text('Message'),
              ),
              PopupMenuItem(
                value: 2,
                child: const Text('Linccup'),
              ),
            ],
            onSelected: (int value){
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (_)=> MessagePage(
                    isPrivate: value == 1,
                  ),
                ),
              );
            },

           
          ),
          floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}

class _MessageBar extends StatelessWidget {
  const _MessageBar(
    this.height,
    this.color,
    this.message,
    {
      Key key,
    }
  ) : super(key: key);

  final double height;
  final Color color;
  final String message;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Container(height: height,color: color, width: 100),
        Text("New Messages"),
        Text("$message"),
      ],
    );
  }
}

class _MessageList extends StatelessWidget {
  const _MessageList({
    Key key,
    @required this.message,
  }) : super(key: key);

  final String message;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      margin: EdgeInsets.all(20),
      alignment: Alignment.center,
      child: Text(
        "$message",
        style: TextStyle(
          color: Colors.grey[600],
          fontSize: 40,
        ),
       ),
      decoration: BoxDecoration(
        color: Colors.green,
        borderRadius: BorderRadius.all(Radius.circular(5.0)),
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [Colors.green,
                   Colors.greenAccent,
                   Colors.lightGreenAccent,
                   ],
          stops: [0.85, 0.95, 1.0],
        ),
        boxShadow: [
          BoxShadow(
            color: Colors.grey,
            offset: Offset(4,4),
          ),
          BoxShadow(
            color: Colors.grey[50],
            offset: Offset(2,2),
          ),
        ],
      ),
    );
  }
}