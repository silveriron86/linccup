import 'package:flutter/material.dart';

final icons = [
  Icons.access_alarm,
  Icons.account_balance,
  Icons.account_balance_wallet,
  Icons.account_box,
  Icons.add_a_photo,
  Icons.apps,
];