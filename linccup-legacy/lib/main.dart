import './application.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';


void main() async {

  

  await SystemChrome.setPreferredOrientations(
    [
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp
    ],
  );
  

  runApp(Application());
}

// class FirebaseService {
//   final StreamController<List<Linccup>> _linccupController = StreamController<List<Linccup>>();

//   FirebaseService(){
//     Firestore.instance.collection('linccups').snapshots().listen(_linccupsAdded);
//   }

//   Stream<List<Linccup>> get linccup => _linccupController.stream; 

//   void _linccupsAdded(QuerySnapshot snapshot){
//     var linccups = _getLinccupsFromSnapshot(snapshot);
//     _linccupController.add(linccups);

//   }

//   List<Linccup> _getLinccupsFromSnapshot(QuerySnapshot snapshot){
//     var linccupItems = List<Linccup>();
//     var documents = snapshot.documents;

//     var hasDocuments  = documents.length > 0;

//     if(hasDocuments){
//       for(var document in documents){
//         var documentData = document.data;
//         documentData['id'] = document.documentID;
//         linccupItems.add(Linccup.fromData(documentData));
//       }
//     }

//     return linccupItems;
//   }
// }

// class MyApp extends StatelessWidget {
//   // This widget is the root of your application.
//   @override
//   Widget build(BuildContext context) {
//     return Provider<FirebaseBloc>(
//         builder: (_) => FirebaseBloc(apis: Apis()),
//         dispose: (_, value) => value.dispose(),
//         child: MaterialApp(
//           title: 'Flutter Demo',
//           theme: ThemeData(
//             // This is the theme of your application.
//             //
//             // Try running your application with "flutter run". You'll see the
//             // application has a blue toolbar. Then, without quitting the app, try
//             // changing the primarySwatch below to Colors.green and then invoke
//             // "hot reload" (press "r" in the console where you ran "flutter run",
//             // or simply save your changes to "hot reload" in a Flutter IDE).
//             // Notice that the counter didn't reset back to zero; the application
//             // is not restarted.
//             primarySwatch: Colors.blue,
//           ),
//           home: BlocProvider<IncrementBloc>(
//             bloc: IncrementBloc(),
//             child: CounterPage(),
//           ),
//         ),
//     );
//   }
// }

// class LinccupApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Provider<FirebaseBloc>(
//         builder: (_) => FirebaseBloc(apis: Apis()),
//         dispose: (_, value) => value.dispose(),
//         child: MaterialApp(
//         title: "Linccup",
//         theme: ThemeData(
//           primarySwatch: Colors.blue,
//         ),
//         home: BlocProvider<LinccupBloc>(
//           bloc: LinccupBloc(),
//           child: HomePage(),
//         ),
//       ),
//     );
//   }
// }

// class HomePage extends StatelessWidget {
  


//   @override
//   Widget build(BuildContext context) {
//     final LinccupBloc bloc = BlocProvider.of<LinccupBloc>(context);
//     return Scaffold(
//       appBar: AppBar(
//         title: Text('Linccups'),
//       ),
//       // body: LinccupListview(),
//       body: LoginPage(),
//       bottomNavigationBar:
//         BottomAppBar(
//           child: Container(
//             height: 50.0,
//             child: StreamBuilder<String>(
//               stream: bloc.outMessage,
//               initialData: 'Macs message',
//               builder: (BuildContext context, AsyncSnapshot<String> snapshot){
//                 return Text('You sent a message ${snapshot.data} text');
//               },
//             ),
//           ),
//           shape: const CircularNotchedRectangle(),
//       ),
//       floatingActionButton:
//         FloatingActionButton(
//           onPressed: () =>  bloc.incrementCounter.add(null),
//           tooltip: "Floating action button",
//           child: Icon(Icons.add),
//         ),
//         floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
//     );
//   }
// }


// class LinccupListview extends StatelessWidget {
//   final linccups = ['Coffee with KR', 'Ping pong coding day', 'support our troops pizza day'];
  
//   @override 
//   Widget build(BuildContext context){
//     return ListView.separated(
//       itemCount: linccups.length,
//       itemBuilder: (context, index) {
//         return Card(
//             child: ListTile(
//               leading: Icon(Icons.add),
//               title: Text(linccups[index]),
//           ),
//         );
//       },
//       separatorBuilder: (context, index){
//         return Divider();
//       },
//     );
//   }
// }


// class CounterPage extends StatelessWidget {

  

//   @override
//   Widget build(BuildContext context){
//     final IncrementBloc bloc = BlocProvider.of<IncrementBloc>(context);
//     return Scaffold(
//       appBar: AppBar(title: Text("Stream version of the counter app"),),
//       body: Center(
//         child: StreamBuilder<int>(
//           stream: bloc.outCounter,
//           initialData: 0,
//           builder: (BuildContext context, AsyncSnapshot<int> snapshot){
//             return Text('You hit me: ${snapshot.data} times');
//           }
//         ),
//       ),
//       floatingActionButton: FloatingActionButton(
//         child: const Icon(Icons.add),
//         onPressed: (){
//             bloc.incrementCounter.add(null);
//         },
//       ),


//       );
//   }
// }

// abstract class BlocBase{
//   void dispose();
// }

// class LinccupBloc implements BlocBase{

//   String _linccup;

//   StreamController<String> _linccupController = StreamController<String>();
//   StreamSink<String> get _inMessage => _linccupController.sink;
//   Stream<String> get outMessage => _linccupController.stream;

//   StreamController _actionController = StreamController();
//   StreamSink get incrementCounter => _actionController.sink;

//   LinccupBloc() {
//     _linccup = '';
//     _actionController.stream
//                      .listen(_handleLogic);
//   }

//   void dispose(){
//     _actionController.close();
//     _linccupController.close();
//   }

//   void _handleLogic(data){
//     _linccup = 'Coffee with KR';
//     _inMessage.add(_linccup);
//   }


// }

// class IncrementBloc implements BlocBase{
//   int _counter;

//   StreamController<int> _counterController = StreamController<int>();
//   StreamSink<int> get _inAdd => _counterController.sink;
//   Stream<int> get outCounter => _counterController.stream;

//   StreamController _actionController = StreamController();
//   StreamSink get incrementCounter => _actionController.sink;

//   IncrementBloc() {
//     _counter = 0;
//     _actionController.stream
//                      .listen(_handleLogic);
//   }

//   void dispose() {
//     _actionController.close();
//     _counterController.close();
//   }

//   void _handleLogic(data){
//     _counter = _counter + 1;
//     _inAdd.add(_counter);
//   }

// }


// class BlocProvider<T extends BlocBase> extends StatefulWidget {
//   BlocProvider({
//     Key key,
//     @required this.child,
//     @required this.bloc,
//   }): super(key: key);

//   final T bloc;
//   final Widget child;

//   @override
//   _BlocProviderState<T> createState() => _BlocProviderState<T>();

//   static T of<T extends BlocBase>(BuildContext context){
//     final type = _typeOf<BlocProvider<T>>();
//     BlocProvider<T> provider = context.ancestorWidgetOfExactType(type);
//     return provider.bloc;
//   }

//   static Type _typeOf<T>() => T;

// }

// class _BlocProviderState<T> extends State<BlocProvider> {
//   @override
//   void dispose(){
//     widget.bloc.dispose();
//     super.dispose();
//   }

//   @override
//   Widget build(BuildContext context){
//     return widget.child;
//   }
// }