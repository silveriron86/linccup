/* eslint-disable promise/no-nesting */
/* eslint-disable promise/always-return */
/* eslint-disable consistent-return */
const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();

exports.getLinccupUser = functions.https.onRequest(async (req, res) => {
  if (typeof req.body.authToken === 'undefined') {
      return res.status(400).send('Auth token is required!');
  }

  admin.auth().verifyIdToken(req.body.authToken)
    .then(decodedToken => {
      const id = decodedToken.email;
      const linccupUsersRef  = admin.firestore().collection('linccupUsers').doc(id);
      linccupUsersRef.get()
        .then(doc => {
          if (!doc.exists) {
            return res.status(400).send('No such linccupUsers document');
          } else {
            return res.json(doc.data());
          }
        }).catch(error => {
          return res.status(400).send(error);
        });
      }).catch(error => {
        return res.status(400).send(error);
      });
});

// Store device token into linccupUsers
exports.storeDeviceToken = functions.https.onRequest(async (req, res) => {
  if (req.method !== 'POST') {
    return res.status(403).send('Forbidden!');
  }
  if (typeof req.body.authToken === 'undefined') {
      return res.status(400).send('Auth token is required!');
  }
  if (typeof req.body.deviceToken === 'undefined') {
    return res.status(400).send('Device token is required!');
  }  

  admin.auth().verifyIdToken(req.body.authToken)
    .then(decodedToken => {
      const id = decodedToken.email;
      const linccupUsersRef  = admin.firestore().collection('linccupUsers').doc(id);
      linccupUsersRef.get()
        .then(doc => {
          if (!doc.exists) {
            console.log('No such linccupUsers document!');
            throw new Error('No such linccupUsers document!');
          } else {
            let deviceToken = doc.data().deviceToken
            if(typeof deviceToken === 'undefined') {
              deviceToken = [];              
            }
            
            const newToken = req.body.deviceToken;
            if (deviceToken.indexOf(newToken) === -1) {
              deviceToken.push(newToken);
              linccupUsersRef.update({
                deviceToken
              });
            }
            return res.json({
              result: 'success'
            });
          }
        }).catch(error => {
          return res.status(400).send(error);
        });
      }).catch(error => {
        return res.status(400).send(error);
      });
});

// Accepts an firebase token and saves the given geopoint as the 'lastKnownLocation' for a user in firebase
exports.storeLocation = functions.https.onRequest(async (req, res) => {
  if (req.method !== 'POST') {
    return res.status(403).send('Forbidden!');
  }
  if (typeof req.body.authToken === 'undefined') {
      return res.status(400).send('Auth token is required!');
  }
  if (typeof req.body.geoPoint === 'undefined') {
      return res.status(400).send('Geo point is required!');
  }

  admin.auth().verifyIdToken(req.body.authToken)
    .then(decodedToken => {
      const id = decodedToken.email;
      admin.firestore().collection('linccupUsers').doc(id).update({
        lastKnownLocation: req.body.geoPoint
      });
      return res.json({
        result: 'success'
      });
    }).catch(error => {
      return res.status(400).send(error);
    });
});

// Takes an auth token for a user and a contact {linccup user} and saves in a collection of contacts for the given linccup user. 
exports.addContact = functions.https.onRequest(async (req, res) => {
  if (req.method !== 'POST') {
    return res.status(403).send('Forbidden!');
  }
  if (typeof req.body.authToken === 'undefined') {
      return res.status(400).send('Auth token is required!');
  }
  if (typeof req.body.contact === 'undefined') {
      return res.status(400).send('Contact is required!');
  }
  
  admin.auth().verifyIdToken(req.body.authToken)
    .then(decodedToken => {
      const id = decodedToken.email;
      const linccupUsersRef  = admin.firestore().collection('linccupUsers').doc(id);
      linccupUsersRef.get()
        .then(doc => {
          if (!doc.exists) {
            console.log('No such linccupUsers document!');
            throw new Error('No such linccupUsers document!');
          } else {
            let contacts = doc.data().contacts
            if(typeof contacts === 'undefined') {
              contacts = [];
            }

            let contact = req.body.contact;
            contact.timestamps = Date.now();
            contacts.push(contact);
            linccupUsersRef.update({
              contacts
            });
            
            return res.json({
              result: 'success'
            });            
          }
        }).catch(error => {
          return res.status(400).send(error)
        })
    }).catch(error => {
      return res.status(400).send(error);
    });   
});

// Takes an Auth token for a user and a linccup request and send a push notification to the contact that was requested.
exports.sendLinccupRequest = functions.https.onRequest(async (req, res) => {
  if (req.method !== 'POST') {
    return res.status(403).send('Forbidden!');
  }
  if (typeof req.body.authToken === 'undefined') {
      return res.status(400).send('Auth token is required!');
  }
  if (typeof req.body.payload === 'undefined') {
      return res.status(400).send('Notification payload is required!');
  }

  admin.auth().verifyIdToken(req.body.authToken)
    .then(decodedToken => {
      const id = decodedToken.email;
      const linccupUsersRef  = admin.firestore().collection('linccupUsers').doc(id);
      linccupUsersRef.get()
        .then(doc => {
          if (!doc.exists) {
            console.log('No such linccupUsers document!');
            throw new Error('No such linccupUsers document!');
          } else {
            let deviceToken = doc.data().deviceToken
            if(typeof deviceToken === 'undefined') {
              return res.status(400).send({
                error: 'The user does not have device token'
              })
            }
            console.log(deviceToken, req.body.payload);
            admin.messaging().sendToDevice(deviceToken, {
              notification: req.body.payload
            });
            
            return res.json({
              result: 'success'
            });            
          }
        }).catch(error => {
          return res.status(400).send(error)
        })
    }).catch(error => {
      return res.status(400).send(error);
    }); 
});

// Takes an auth token for a user and a linccup request { linccup type ( selected from the 3 options in the Invite linccup screen) date, time, location, message, linccup status{ accepted, completed, cancelled},
exports.createLinccup = functions.https.onRequest(async (req, res) => {
  if (req.method !== 'POST') {
    return res.status(403).send('Forbidden!');
  }
  if (typeof req.body.authToken === 'undefined') {
      return res.status(400).send('Auth token is required!');
  }
  if (typeof req.body.linccup === 'undefined') {
      return res.status(400).send('Linccup is required!');
  }

  admin.auth().verifyIdToken(req.body.authToken)
    .then(decodedToken => {
      const id = decodedToken.email;
      const linccupUsersRef  = admin.firestore().collection('linccupUsers').doc(id);
      linccupUsersRef.get()
        .then(doc => {
          if (!doc.exists) {
            console.log('No such linccupUsers document!');
            throw new Error('No such linccupUsers document!');
          } else {
            let linccups = doc.data().linccups
            if(typeof linccups === 'undefined') {
              linccups = [];
            }

            linccups.push(req.body.linccup);
            linccupUsersRef.update({
              linccups
            });
            
            return res.json({
              result: 'success'
            });            
          }
        }).catch(error => {
          return res.status(400).send(error)
        })
    }).catch(error => {
      return res.status(400).send(error);
    });
});
